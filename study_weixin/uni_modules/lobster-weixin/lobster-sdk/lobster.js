import throttle from './js/throttle.js'
import util_js from './js/util.js'
import login_js from './js/login.js'
import user_js from './js/user.js'
import config_js from './js/config.js'
const wxlobsterapp = {
	utils: util_js,
	login:login_js,
	user:user_js,
	config:config_js,
	
	/**
	 * @description 隐藏手机号码
	 * @param {Object} phone
	 */
	hidePhone(phone) {
	  let reg = /^(\d{3})\d{4}(\d{4})$/;
	  return phone.replace(reg, '$1****$2');
	},
}
export default wxlobsterapp;
