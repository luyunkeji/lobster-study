import http from './http.js'
import config from './config.js'
export default {
	/**
	 * 微信授权
	 * **/
	wxAuth() {
		uni.setStorageSync('wxcode', '');
		return new Promise((resolve, reject) => {
			// #ifdef APP-PLUS
			weixinAuthService.authorize(function(res) {
				uni.setStorageSync('wxcode', res.code);
				resolve(res.code)
			}, function(err) {
				console.error(err)
				reject(new Error('微信授权失败'))
			});
			//#endif
			//#ifdef MP-WEIXIN
			uni.login({
				provider: 'weixin',
				success(res) {
					uni.setStorageSync('wxcode', res.code);
					resolve(res.code)
				},
				fail(err) {
					console.error('微信授权失败：' + JSON.stringify(err));
					reject(new Error('微信授权失败'))
				}
			})
			// #endif
		})
	},
	/**
	 * 微信快速登录
	 * **/
	wxLogin(code,appId,workId) {
		return http.GET(config.apiGateway +  "/sso/v1/mpLogin/wxLogin", {
			wxcode: code,
			appId: appId || config.appId,
			workId: workId ||  config.workId,
		})
	},
	/**
	 * 注册
	 * **/
	wxRegister(iv, encryptedData,appId,workId) {
		console.log(iv);
		console.log(encryptedData);
		return http.GET(config.apiGateway +  '/sso/v1/mpLogin/wxRegister', {
			wxcode: uni.getStorageSync('wxcode'),
			appId: appId || config.appId,
			iv: iv,
			encryptedData: encryptedData,
			workId: workId || config.workId
		})
	},
	/**
	 * APP微信快速登录
	 * **/
	appWxLogin(openId, unionId,workId) {
		return http.GET(config.apiGateway + '/sso/v1/MPLogin/appWxLogin', {
			openId: openId,
			unionId: unionId,
			workId: workId || config.workId,
		});
	},
	/**
	 * 发送验证码
	 * **/
	sendMsgCode(phone, key) {
		return http.GET(config.apiGateway +'/sso/v1/Login/SendMsgCode', {
			phone: phone,
			key: key,
		});
	},
	/**
	 * 手机一键登录
	 * **/
	loginBySms(phone, code, smskey) {
		console.log("手机一键登录" + phone);
		return http.GET(config.apiGateway +'/sso/v1/MPLogin/phoneLogin', {
			phone: phone,
			code: code,
			key: smskey,
			workId: config.workId
		});
	},
	
	/**
	 * 退出登录
	 */
	logout(){
		
	},
}