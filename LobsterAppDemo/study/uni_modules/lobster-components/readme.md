# lobster-components

##注意事项：
##如果是单独导入组件的情况下需要更改部分文件
##1，store/index.js 
import group from '@/uni_modules/lobster-components/modules/group'
import user from '@/uni_modules/lobster-components/modules/user'
import login from '@/uni_modules/lobster-components/modules/login.js'
import im from '@/uni_modules/lobster-components/lobster-sdk/js/im.js'

export default new Vuex.Store({
	modules: {
		login, 
		group,
		user, 
		im
	}
})

##2,main.js
import store from './store'
import util from '@/uni_modules/lobster-components/lobster-sdk/js/util.js'
import  lobster from '@/uni_modules/lobster-components/lobster-sdk/lobster.js'

Vue.prototype.$util = util;
Vue.prototype.$store = store;
Vue.prototype.$lobster = lobster;

export function createApp() {
  const app = createSSRApp(App)
  return {
	  store,
      app
  }
}

## 主要包含功能
## 一、即时通讯
## 二、音视频
## 三、直播
## 四、视频播放器
## 五、相机
## 六、签名
## 七、投屏