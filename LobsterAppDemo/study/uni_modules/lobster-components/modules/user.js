const userModules = {
	state: {
		updatePatient: 0,
		myInfo: {},
		userProfile: {},
		blacklist: []
	},
	getters: {
		myInfo: state => state.myInfo,
		userProfile: state => state.userProfile
	},
	mutations: {
		updatePatient(state){
			state.updatePatient=state.updatePatient+1
		},
		updateMyInfo(state, myInfo) {
			state.myInfo = myInfo
		},
		updateUserProfile(state, userProfile) {
			state.userProfile = userProfile
		},
		setBlacklist(state, blacklist) {
			state.blacklist = blacklist
		},
		updateBlacklist(state, blacklist) {
			state.blacklist.push(blacklist)
		},
		resetUser(state) {
			state.blacklist = []
			state.userProfile = {}
			state.myInfo = {}
		}
	}
}

export default userModules
