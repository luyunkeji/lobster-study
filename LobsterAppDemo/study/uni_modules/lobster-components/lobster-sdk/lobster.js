import power from './js/permission.js'
import throttle from './js/throttle.js'
import userlist from './js/userlist.js'
import coundinfo from './js/clound.js'
import https from './js/http.js'
import imEntity from './js/imEntity.js'
import imJs from './js/im.js'
import imCommon from './js/imCommon.js'
import imLogin from './js/imLogin.js'
import ux_logJs from './js/logJs.js'
import ux_utils from './js/util.js'

// #ifdef APP-PLUS
const tim = uni.requireNativePlugin("ux-tim");
const ux_live = uni.requireNativePlugin("ux-live");
const ux_sign = uni.requireNativePlugin('ux-sign');
const ux_jyrtc = uni.requireNativePlugin("JY-TXRTC");
const ux_lyzmlDLNA = uni.requireNativePlugin("lyzml-DLNA");
const ux_camera = uni.requireNativePlugin("ux-camera");
// #endif



const lobsterapp = {
	userdata: userlist,
	clound: coundinfo,
	logId:0,
	getLogId:function(){
		this.logId =this.logId +1;
		return this.logId;
	},
	log:{
		logdata:ux_logJs,
		/**
		 * @description  写日志
		 * @param {Object} info  
		 * type:类型  
		 * logId: 编号  可以为空  
		 *  msg:消息内容  
		 * reqdata:请求内容   用于post 或者get请求
		 * respdata: 返回内容 用于post 或者get请求
		 */
		writeLog(info){
			ux_logJs.logs.push({
				type:info.type,//http类型日志
				id:info.id || lobsterapp.getLogId(),
				time: ux_utils.getCurrentTime(),
				msg: info.msg,
				reqdata: info.reqdata,
				respdata:info.respdata,//'请求中'
			});
		},
		/**
		 * @description  普通日志
		 * @param {Object} msg  日志内容
		 */
		info(msg){
			ux_logJs.logs.push({
				type:0,//普通日志
				id:lobsterapp.getLogId(),
				time: ux_utils.getCurrentTime(),
				msg: msg,
				reqdata: '',
				respdata:'',//'请求中'
			});
		},
		/**
		 * @description 错误日志
		 * @param {Object} msg 内容
		 */
		error(msg){
			ux_logJs.logs.push({
				type:2,//普通日志
				id:lobsterapp.getLogId(),
				time: ux_utils.getCurrentTime(),
				msg: msg,
				reqdata: '',
				respdata:'',
			});
		},
		/**
		 * @description  清除日志
		 */
		clearLog(){
			lobsterapp.logId=0;
			ux_logJs.clearLog();
		},
	},
	
	/**
	 * @description request 请求
	 */
	http: {
		/**
		 * @param {Object} url url 地址
		 * @param {Object} data 数据
		 * @param {Object} content_type 请求类型
		 */
		get(url, data, content_type) {
		   let logId =lobsterapp.getLogId();
		    lobsterapp.log.writeLog({type:1,id:logId,msg:url,reqdata:data,respdata:'请求中'});
			
			return https.GET(url, data, content_type).then(res=>{
				ux_logJs.setrespdata(logId,res);
				return res;
			}).catch(res=>{
				ux_logJs.setrespdata(logId,res);
				console.log(res)
			    throw new Error(res);
			});
		},
		/**
		 * @param {Object} url url 地址
		 * @param {Object} data 数据
		 * @param {Object} content_type 请求类型
		 */
		post(url, data, content_type) {
			let logId =lobsterapp.getLogId();
			lobsterapp.log.writeLog({type:1,id:logId,msg:url,reqdata:data,respdata:'请求中'});
			
			return https.POST(url, data, content_type).then(res=>{
				ux_logJs.setrespdata(logId,res);
				return res;
			}).catch(res=>{
				ux_logJs.setrespdata(logId,res);
				return res;
			});
		},
	},

	/**
	 * @description  授权相关
	 */
	permission: {
		/**
		 * 授权
		 * @param {Object} permissionID 授权编号
		 * @param {Object} firstFun   回调
		 */
		auth(permissionID, firstFun) {
			return power.auth(permissionID, firstFun);
		},
		/**
		 * @description  去授权
		 */
		gotoAppPermissionSetting() {
			power.gotoAppPermissionSetting();
		},
	},

	/**
	 * 实时通讯对象
	 */
	im: {
		videoInfo: imEntity.videoInfo,
		textInfo: imEntity.textInfo,
		imgInfo: imEntity.imgInfo,
		voiceInfo: imEntity.voiceInfo,
		signalingInfo: imEntity.signalingInfo,
		onlineEmoji: imEntity.onlineEmoji,
		emojiList: imEntity.emojiList,
		/**
		 * 初始化
		 * @sdkAppID 腾讯云sdk
		 * @callback 回调
		 */
		initSDK(callback) {
			imLogin.initSDK(callback);
		},
		/**
		 * 登录
		 * userId 用户id
		 * userSig 用户标识
		 * @callback 回调
		 */
		login(userId, callback) {
			imLogin.login(userId, callback);
		},
		/**
		 * 退出登录
		 */
		loginout() {
			imLogin.loginout();
		},
		/**
		 * 发送图片消息
		 * @data  {userID imgPath busnessId} 图片信息{对方userid、图片路径、业务编号id}
		 * @callback 回调
		 */
		sendC2CImgMessage(groupId, imagePath, userID, cloudCustomData, callback) {
			tim.sendC2CImageMessage(groupId, imagePath, userID, cloudCustomData, res => {
				if (callback) {
					callback(res);
				}
			})
		},

		/**
		 * 发送文本消息
		 * @data  {userID text busnessId} 文本消息信息{对方userid、文本内容、业务编号id}
		 * @callback 回调
		 */
		sendC2CTextMessage(groupId, text, userID, cloudCustomData, callback) {
			tim.sendC2CTextMessage(groupId, text, userID, cloudCustomData, res => {
				console.log(res)
				if (callback) callback(res);
			})
		},

		/**
		 * 发送语音消息
		 * @data  {soundPath duration  userID busnessId} 音频地址、时长、对方用户id、 业务编号id
		 * @callback 回调
		 */
		sendSoundMessage(groupId, soundPath, duration, userID, cloudCustomData, callback) {
			tim.sendSoundMessage(groupId, soundPath, duration, userID, cloudCustomData, result => {
				if (callback) callback(result);
			});
		},

		/**
		 * 发送短视频消息
		 * @data  {videoFilePath duration  userID type busnessId} 短视频地址、时长、对方用户id、类型、 业务编号id
		 * @callback 回调
		 */
		sendVideoMessage(groupId, videoFilePath, duration, type, snapshotPath, userID, cloudCustomData, callback,
			proback) {
			//console.log(data);
			tim.sendVideoMessage(groupId, videoFilePath, duration, type, snapshotPath, userID, cloudCustomData,
				result => {
					callback(result);
				}, pro => {
					proback(pro);
				})
		},
		/**
		 *  获取信令信息
		 */
		getSignalingInfo(msgId, callback) {
			tim.getSignalingInfo(msgId, res => {
				//console.log(res);
				if (callback) callback(res);
			});
		},
		//自定义消息
		getCustomMessageData2String(msgId, callback) {
			tim.getCustomMessageData2String(msgId, res => {
				//console.log(res);
				if (callback) callback(res);
			});
		},

		/**
		 * 邀请视频通话
		 */
		invite(userId, callback) {
			tim.invite(userId, '2049', false, 60, res => {
				callback(res);
			});
		},

		/**
		 * 获取短视频封面
		 * @callback 回调
		 */
		getVideoThumbnail(url) {
			return new Promise((resolve, reject) => {
				try {
					console.log("准备获取封面");
					tim.GetVideoThumbnail(url, res => {
						console.log("准备获取封面准备返回");
						console.log(res);
						if (res.code == 0) {
							resolve(res);
						} else {
							reject(res)
						}
					})
				} catch (res) {
					console.log(res);
					uni.showToast({
						title: JSON.stringify(res)
					})
					reject(res);
				}
			});
		},
		/**
		 * @param {Object} url 保存文件
		 */
		saveFile(url) {
			return new Promise((resolve, reject) => {
				try {
					uni.saveFile({
						tempFilePath: url,
						success: function(res) {
							var savedFilePath = res.savedFilePath;
							resolve(savedFilePath)
						},
						fail: function(res) {
							// uni.showModal({
							// 	 title :"saveFileFail",
							// 	 content:JSON.stringify(res)
							// });
							console.log(res);
							resolve("");
						}
					});
				} catch (res) {
					uni.showModal({
						title: "saveFileError",
						content: JSON.stringify(res)
					});
					console.log(res);
					reject(res);
				}
			});
		},


		/**
		 * 事件监听
		 * @callback 回调
		 */
		addAdvancedMsgListener(callback) {
			tim.addAdvancedMsgListener(res => {
				if (callback) callback(res);
			})
		},

		/**
		 * 信令监听
		 * @callback 回调
		 */
		addSignalingListener(callback) {
			tim.addSignalingListener(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 移除监听
		 * @callback 回调
		 */
		removeSignalingListener(callback) {
			tim.removeSignalingListener(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 邀请
		 * @callback 回调
		 * @invite 	被邀请人用户 ID
		 * @data 自定义数据
		 * @onlineUserOnly 是否只有在线用户才能收到邀请，如果设置为 true，
		 * 只有在线用户才能收到， 并且 invite 操作也不会产生历史消息
		 * （针对该次 invite 的后续 cancel、accept、reject、timeout 操作也同样不会产生历史消息）
		 * @timeout 超时时间，单位秒，如果设置为 0，SDK 不会做超时检测，
		 * 也不会触发 onInvitationTimeout 回调
		 */
		invite(invitee, data, onlineUserOnly, timeout, callback) {
			tim.invite(invitee, data, onlineUserOnly, timeout, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 邀请方取消邀请
		 * 如果所有被邀请人都已经处理了当前邀请（包含超时），不能再取消当前邀请。
		 *
		 * @param inviteId  	邀请 ID  发送邀请后返回编号
		 * @param data 自定义数据
		 * @param callback
		 */
		cancelInvite(inviteId, callback) {
			tim.cancelInvite(inviteId, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 接收方接收邀请
		 * 不能接受不是针对自己的邀请，
		 * 请在收到 onReceiveNewInvitation 回调的时候先判断 inviteeList 有没有自己，
		 * 如果没有自己，不能 accept 邀请。
		 *
		 * @param inviteId
		 * @param data
		 * @param callback
		 */
		acceptInvite(inviteId, data, callback) {
			tim.acceptInvite(inviteId, data, res => {
				if (callback) callback(res);
			})
		},

		/**
		 * 接收方拒绝邀请
		 * 不能拒绝不是针对自己的邀请，
		 * 请在收到 onReceiveNewInvitation 回调的时候先判断 
		 * inviteeList 有没有自己，如果没有自己，不能 reject 邀请。
		 * @param inviteId
		 * @param data
		 * @param callback
		 */
		rejectInvite(inviteId, data, callback) {
			tim.rejectInvite(inviteId, data, res => {
				if (callback) callback(res);
			})
		},

		/**
		 * 获取历史数据
		 * @touserId 对方用户编号
		 * @count 数量
		 * @lastMsgId 最后一条数据对象
		 * @callback 回调
		 */
		getC2CHistoryMessageList(touserId, count, lastMsgId, callback) {
			console.log(touserId + '_' + count + '_' + lastMsgId);
			try {
				tim.getC2CHistoryMessageList(touserId, count, lastMsgId, res => {
					if (callback) callback(res);
				})
			} catch (res) {
				console.log(res);
				uni.showToast({
					title: JSON.stringify(res)
				})
			}
		},

		/**
		 * 移除监听
		 */
		removeAdvancedMsgListener() {
			tim.removeAdvancedMsgListener();
		},
		/**
		 * 获取会话列表
		 */
		getConversationList(nextSeq, count, callback) {
			tim.getConversationList(nextSeq, count, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * 设置会话监听
		 */
		setConversationListener(callback) {
			tim.setConversationListener(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 删除会话
		 */
		deleteConversation(conversationID, callback) {
			tim.deleteConversation(conversationID, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * 获取会话未读总数
		 */
		getTotalUnreadMessageCount(callback) {
			tim.getTotalUnreadMessageCount(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 设置消息已读
		 */
		markC2CMessageAsRead(userID, callback) {
			tim.markC2CMessageAsRead(userID, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 设置消息置顶
		 */
		pinConversation(conversationID, isPinned, callback) {
			tim.pinConversation(conversationID, isPinned, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 退出登录
		 * @callback 回调
		 */
		logout(callback) {
			tim.logout(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * 获取登录状态
		 */
		getLoginStatus() {
			return tim.getLoginStatus();
		},
		/**
		 * 获取当前登录用户
		 */
		getLoginUser() {
			return tim.getLoginUser();
		},
		/**
		 * 卸载sdk
		 * @callback 回调
		 */
		unInitSDK(callback) {
			tim.unInitSDK(res => {
				if (callback) callback();
			});
		},

		/**
		 * 下载图片
		 * @callback 调用回调
		 * @downCallback 下载回调
		 */
		downloadImage(msgId, callback, downCallback) {
			tim.downloadImage(msgId,
				back => {
					if (callback) callback(back);
				}, downback => {
					if (downCallback) downCallback(downback);
				});
		},
		/**
		 * 下载语音
		 * @callback 调用回调
		 * @downCallback 下载回调
		 */
		downloadSound(msgId, callback, downCallback) {
			tim.downloadSound(msgId,
				back => {
					if (callback) callback(back);
				}, downback => {
					if (downCallback) downCallback(downback);
				});
		},

		/**
		 * 下载短视频
		 * @callback 调用回调
		 * @downCallback 下载回调
		 */
		downloadVideo(msgId, callback, downCallback) {
			tim.downloadVideo(msgId,
				back => {
					if (callback) callback(back);
				}, downback => {
					if (downCallback) downCallback(downback);
				});
		},

		/**
		 * 下载短视频封面
		 * @callback 调用回调
		 * @downCallback 下载回调
		 */
		downloadVideoImg(msgId, callback, downCallback) {
			tim.downloadVideoImg(msgId,
				back => {
					if (callback) callback(back);
				}, downback => {
					if (downCallback) downCallback(downback);
				});
		},

		/**
		 * 撤回消息
		 * @callback 调用回调
		 */
		revokeMessage(msgId, callback) {
			tim.revokeMessage(msgId, res => {
				if (callback) callback(res);
			})
		},

		/**
		 * 生成本地的一个唯一标识
		 */
		localCustomData(userId, touserId, busnessId) {
			return userId + '_' + touserId + '_' + busnessId + '_' + new Date().getTime();
		},

		/**
		 * 获取图片信息
		 * @callback 回调
		 */
		getImageInfo(url) {
			return new Promise((resolve, reject) => {
				try {
					uni.getImageInfo({
						src: url,
						success: res => {
							resolve(res);
						},
						fail: res => {
							reject(res);
						}
					})
				} catch (res) {
					reject(res);
				}
			});
		},



		/**@dateTimeFliter 转换格林日期时间格式为常用日期格式
		 * @time[必填] 						Date  		格林日期格式
		 * @part[可选,默认:0]				Number      选择返回日期时间部分  列:0:返回所有 1:只返回日期  2:只返回时间
		 * @dateComplete[可选,默认:true] 	Boolean 	日期位数不足是否添0补齐:true:补齐,false:不补齐
		 * @timeComplete[可选,默认:true] 	Boolean 	时间位数不足是否添0补齐:true:补齐,false:不补齐
		 * @dateConnector[可选,默认:-] 		String 		年月日连接符  例: - : /
		 * @timeConnector[可选,默认::] 		String 		时间连接符   例: - : /
		 * @hour12[可选,默认:false]          Boolean     是否返回12小时制时间   例: true:返回12小时制时间   false:返回24小时制时间
		 * @return   '2019-11-25 15:05:54'  String    返回示例
		 * **/
		dateTimeFliter(time, part = 0, dateComplete = true, timeComplete = true, dateConnector = '-',
			timeConnector =
			':', hour12 = false) {
			// 	console.log(time);
			// time = time.replace(/-/g, '/')
			// console.log(time);
			//console.log(time)
			let year = time.getFullYear()
			let month = time.getMonth() + 1
			let day = time.getDate()
			let hour = time.getHours()
			let minute = time.getMinutes()
			let second = time.getSeconds()
			let dateStr = ''
			let timeStr = ''
			//转换日期
			if (dateComplete) { //添0补齐
				if (month < 10) {
					month = '0' + month
				}
				if (day < 10) {
					day = '0' + day
				}
			}

			//转换时间
			//修改小时制
			if (hour12) {
				if (hour > 12) {
					hour = hour - 12
					if (timeComplete) {
						if (hour < 10) {
							hour = '下午 ' + '0' + hour
						} else {
							hour = '下午 ' + hour
						}
					}
				} else {
					if (timeComplete) {
						if (hour < 10) {
							hour = '上午 ' + '0' + hour
						} else {
							hour = '上午 ' + hour
						}
					}
				}
			}
			//判断分钟与秒
			if (timeComplete) { //添0补齐
				if (minute < 10) {
					minute = '0' + minute
				}
				if (second < 10) {
					second = '0' + second
				}
			}
			timeStr = hour + timeConnector + minute;
			//合成输出值
			if (part == 0) {
				dateStr = year + '年' + month + '月' + day + '日';
				return dateStr + ' ' + timeStr + timeConnector + second;
			} else if (part == 1) {
				dateStr = year + '年' + month + '月' + day + '日'
				return dateStr
			} else if (part == 2) {
				return timeStr + timeConnector + second;
			} else if (part == 3) {
				return timeStr;
			} else if (part == 4) {
				dateStr = month + '月' + day + '日';
				return dateStr + ' ' + timeStr;
			}
			return '传参有误'
		},
		//替换表情符号为图片
		replaceEmoji(str) {
			let replacedStr = str.replace(/\[([^(\]|\[)]*)\]/g, (item, index) => {
				console.log("item: " + item);
				for (let i = 0; i < this.emojiList.length; i++) {
					let row = this.emojiList[i];
					for (let j = 0; j < row.length; j++) {
						let EM = row[j];
						if (EM.alt == item) {
							//在线表情路径，图文混排必须使用网络路径，请上传一份表情到你的服务器后再替换此路径 
							//比如你上传服务器后，你的100.gif路径为https://www.xxx.com/emoji/100.gif 则替换onlinePath填写为https://www.xxx.com/emoji/
							let onlinePath = 'https://s2.ax1x.com/2019/04/12/'
							let imgstr = '<img src="' + onlinePath + this.onlineEmoji[EM.url] + '">';
							//let imgstr = '<img src="/static/img/emoji/' + EM.url + '">';
							console.log("imgstr: " + imgstr);
							return imgstr;
						}
					}
				}
			});
			return replacedStr;
		},

		getTimestamp() { //把时间日期转成时间戳
			return new Date().getTime() / 1000;
		},
		getCurrentTime() {
			//获取当前时间并打印
			var time;
			var date = new Date();
			let yy = date.getFullYear();
			let mm = date.getMonth() + 1;
			let dd = date.getDate();
			let hh = date.getHours();
			let mf = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
			let ss = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
			time = yy + '/' + mm + '/' + dd + ' ' + hh + ':' + mf + ':' + ss;
			return time;
		},
		//信令消息转换器
		actionConverter(actionType, data) {
			let cType = '';
			if (data) {
				data = JSON.parse(data);
				let tp = data.callType;
				if (tp == 'audio') {
					cType = '语音';
				} else if (tp == 'video') {
					cType = '视频';
				}
			}
			switch (actionType) {
				case 1:
					return '对方发起了' + cType + '邀请';
					break;
				case 2:
					return '对方取消了' + cType + '邀请';
					break;
				case 3:
					return '您已接受了' + cType + '邀请';
					break;
				case 4:
					return '您已拒绝了' + cType + '邀请';
					break;
				case 5:
					return cType + '邀请超时';
					break;
				default:
					return '其他';
					break;
			}
		},
		//错误码转换
		codeConverter(code) {
			switch (code) {
				case 20016:
					return '消息撤回超过了时间限制';
					break;
				case 6014:
					return '未登录，请先登录';
					break;
				case 9523:
					return '联网失败';
					break;
				case 9525:
					return '检测到没有联网';
					break;
				case 9508:
					return '网络已断开';
					break;
				case 6014:
					return '未登录或已下线';
					break;

				case 6206:
					return 'UserSig过期';
					break;
				case 6208:
					return '账号被下线';
					break;
				case 10007:
					return '您已退出群聊,无法发送信息';
					break;
				default:
					return '操作失败_' + code;
					break;
			}
		},
		//比较两个时间相差多少分钟
		compareStamp(timestamp1, timestamp2) {
			let disparity = timestamp1 - timestamp2;
			console.log(disparity);
			let min = Math.round(disparity / 1000 / 60);
			console.log(min);
			return min;
		},

		//计算语音消息长度
		soundLength(voicePlayTimes, screenWidth) {
			var barLen = 0;
			var Lmin = screenWidth * 0.2;
			var Lmax = screenWidth * 0.5;
			var barCanChangeLen = Lmax - Lmin;
			if (voicePlayTimes > 11) {
				barLen = Lmin + voicePlayTimes * 0.05 * barCanChangeLen; // VoicePlayTimes 为10秒时，正好为可变长度的一半
			} else {
				barLen = Lmin + 0.5 * barCanChangeLen + (voicePlayTimes - 10) * 0.01 * barCanChangeLen;
			}
			//console.log(screenWidth + '_' + barLen + '_' + Lmin + '_' + Lmax);
			return barLen;
		},
		//设置角标
		setBadgeNum(num) {
			tim.setBadgeNum(num);
		},

		//----群组相关--------//
		//获取群组消息
		getGroupHistoryMessageList(groupId, count, lastMsgId, callback) {
			try {
				tim.getGroupHistoryMessageList(groupId, count, lastMsgId, res => {
					if (callback) callback(res);
				})
			} catch (res) {
				console.log(res);
				uni.showToast({
					title: JSON.stringify(res)
				})
			}
		},
		/**
		 * 设置群消息已读
		 */
		markGroupMessageAsRead(groupId, callback) {
			tim.markGroupMessageAsRead(groupId, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 退出群组
		 */
		quitGroup(groupId, callback) {
			tim.quitGroup(groupId, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * 发送 @ 信息
		 * @param {type} groupId 群组编号
		 * @param {type} text 文本 
		 * @param atStr @ 的人员
		 */
		sendAtTextMessage(groupId, text, atStr, cloudCustomData, callback) {
			tim.sendAtTextMessage(groupId, text, atStr, cloudCustomData, res => {
				console.log(res)
				if (callback) callback(res);
			})
		},
		/**
		 * 群组监听
		 */
		setGroupListener(callback) {
			tim.setGroupListener(res => {
				if (callback) callback(res);
			})
		},

	},

	/**
	 * 
	 *@description   直播相关
	 */
	live: {
		/** 直播登录
		 * @param {Object} sdkAppId
		 * @param {Object} userId
		 * @param {Object} userSig
		 * @param {Object} callback
		 */
		login(sdkAppId, userId, userSig, callback) {
			ux_live.login(sdkAppId, userId, userSig, res => {
				callback(res);
			});
		},
		/**
		 * 停止推流
		 */
		stopPublish() {
			ux_live.stopPublish();
		},

		/** 发送房间消息
		 * @param {Object} flag
		 * @param {Object} text
		 * @param {Object} callback
		 */
		sendRoomCustomMsg(flag, text, callback) {
			ux_live.sendRoomCustomMsg(flag, text, res => {
				callback(res);
			})
		},
		/**
		 * @description  事件监听
		 * @param {Object} callback
		 */
		setDelegate(callback) {
			ux_live.setDelegate(res => {
				callback(res);
			});
		},
		/**
		 * @description 创建直播房间
		 * @param {Object} roomId
		 * @param {Object} roomName
		 * @param {Object} callback
		 */
		createRoom(roomId, roomName, callback) {
			ux_live.createRoom(roomId, roomName, res => {
				if (callback) callback(res);
			});
		},

		/**
		 * 开启推流
		 * @param {Object} name
		 * @param {Object} callback
		 */
		startPublish(name, callback) {
			ux_live.startPublish(name, res => {
				if (callback) callback(res);
			});
		},
		/** 
		 * @description 進入直播间
		 * @param {Object} roomId
		 * @param {Object} callback
		 */
		enterRoom(roomId, callback) {
			ux_live.enterRoom(roomId, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description  退出直播间
		 * @param {Object} callback
		 */
		exitRoom(callback) {
			ux_live.exitRoom(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 注销房间
		 * @param {Object} callback
		 */
		destroyRoom(callback) {
			ux_live.destroyRoom(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description  申請连麦
		 * @param {Object} title
		 * @param {Object} timeout
		 * @param {Object} callback
		 */
		requestJoinAnchor(title, timeout, callback) {
			ux_live.requestJoinAnchor(title, timeout, res => {
				if (callback) callback(res);
			});
		},

		/**
		 * @description 踢出连麦人员
		 * @param {Object} userId
		 * @param {Object} callback
		 */
		kickoutJoinAnchor(userId, callback) {
			ux_live.kickoutJoinAnchor(userId, res => {
				if (callback) callback(res);
			});
		},

		/**
		 * @description   响应连麦
		 * @param {Object} userId
		 * @param {Object} flag 0, '同意连麦'  1, '不同意连麦'
		 * @param {Object} content
		 */
		responseJoinAnchor(userId, flag, content, callback) {
			ux_live.responseJoinAnchor(userId, flag, content, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description  切换摄像头
		 */
		switchCamera() {
			ux_live.switchCamera();
		},
		 
		/**
		 * @description 静音
		 */
		muteLocalAudio(mute) {
			ux_live.muteLocalAudio(mute);
		},

		/**
		 * @description  发送文本消息
		 * @param {Object} text
		 * @param {Object} callback
		 */
		sendRoomTextMsg(text, callback) {
			ux_live.sendRoomTextMsg(text, res => {
				if (callback) callback(res)
			})
		},
	},

	/**
	 * @description  签名相关
	 */
	sign: {
		/**
		 *@description  设置厂商标识ID
		 * @param {Object} clientId 厂商标识id
		 * @param {Object} callback
		 */
		setClientId(clientId, callback) {
			ux_sign.SetClientId(clientId, res => {
				if (callback) callback(res);
			})
		},

		/**
		 * @description  设置sdk的服务地址
		 * @param {Object} flag 0 正式  1 集成
		 * @param {Object} callback
		 */
		setServerUrl(flag, callback) {
			ux_sign.SetServerUrl(flag, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description 获取sdk的服务地址类型
		 * @param {Object} callback
		 */
		getServerEnvType(callback) {
			ux_sign.GetServerEnvType(res => {
				if (callback) callback(res);
			})
		},

		/**
		 * @description 下载证书
		 * @param {Object} phone 手机号码
		 * @param {Object} callback
		 */
		downCert(phone, callback) {
			ux_sign.DownCert(phone, res => {
				if (callback)
					callback(res);
			})
		},

		/**
		 * @description 更新证书
		 * @param {Object} callback
		 */
		updateCert(callback) {
			ux_sign.UpdateCert(res => {
				if (callback) callback(res);
			});
		},

		/**
		 * @description 证书密码重置
		 * @param {Object} callback
		 */
		resetpin(callback) {
			ux_sign.Resetpin(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description 查看证书
		 * @param {Object} callback
		 */
		showCert(callback) {
			ux_sign.ShowCert(res => {
				if (callback) callback(res);
			})
		},

		/**
		 * 清除本地证书
		 */
		clearCert(callback) {
			ux_sign.ClearCert(res => {
				if (callback) callback(res);
			})
		},

		/**
		 * @description 本地是否下载了证书
		 * @param {Object} callback
		 */
		existsCert(callback) {
			ux_sign.ExistsCert(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 获取证书用户信息
		 * @param {Object} callback
		 */
		getUserInfo(callback) {
			ux_sign.GetUserInfo(res => {
				if (callback) callback(res);
			});
		},

		/**
		 * @description 获取本地证书用户唯一标识openId
		 * @param {Object} callback
		 */
		getOpenId(callback) {
			ux_sign.GetOpenId(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 获取本地证书用户个人签章图片
		 * @param {Object} callback
		 */
		getStampPic(callback) {
			ux_sign.GetStampPic(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description  设置个人签章
		 * @param {Object} callback
		 */
		drawStamp(callback) {
			ux_sign.DrawStamp(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @param {Object} data
		 * @param {Object} callback
		 */
		signData(data, callback) {
			ux_sign.Sign(data, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 开启免密签名
		 *仅支持开启1到60天的时间范围，在处于免密状态下也能调用此接口
		 *调用接口成功后会弹出密码输入框，输入正确密码后，免密功能开启
		 *在免密期间，调用该接口依旧会弹出密码输入框
		 *免密时间以最后一次调用接口传入的keepDay为准
		 * @param {Object} keepDay
		 * @param {Object} callback
		 */
		keepPin(keepDay, callback) {
			ux_sign.KeepPin(keepDay, res => {
				if (callback) callback(res)
			});
		},
		/**
		 * @param {Object} callback
		 */
		isPinExempt(callback) {
			ux_sign.IsPinExempt(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description 清除用户设置的免密签名
		 * @param {Object} callback
		 */
		clearPin(callback) {
			ux_sign.ClearPin(res => {
				if (callback) callback(res);
			})
		},

		/**
		 * @description 请求开启自动签名
		 * @param {Object} sysTag 开启成功后，服务端同步待签数据携带sysTag，该条订单会在服务端通过个人托管证书完成签名
		 * @param {Object} callback
		 */
		signForSignAuto(sysTag, callback) {
			ux_sign.SignForSignAuto(sysTag, res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description 获取自动签名状态
		 * @param {Object} callback
		 */
		signAutoInfo(callback) {
			ux_sign.SignAutoInfo(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description  停止自动签名
		 * @param {Object} callback
		 */
		stopSignAuto(callback) {
			ux_sign.StopSignAuto(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description  获取当前版本号
		 * @param {Object} callback
		 */
		getVersion(callback) {
			ux_sign.GetVersion(res => {
				if (callback) callback(res);
			})
		},
	},

	/**
	 * @description  视频相关
	 */
	jyrtc: {
		/**
		 * @description  SDK初始化，请注意：后续相关的数据都是在此方法的result里面返回
		 * @param {Object} userid
		 * @param {Object} callback
		 */
		initPlugin(userid, callback) {
			ux_jyrtc.initPlugin({
				userid: userid
			}, res => {
				callback && callback(res);
			})
		},
		/**
		 * @description  SDK注册相关方法，请注意：必须调用！！！
		 * @param {Object} result
		 * @param {Object} callback
		 */
		registerFunc(result, callback) {
			ux_jyrtc.registerFunc(result, res => {
				callback && callback(res);
			})
		},
		/**
		 * @description  进入房间  房间相关处理结果均在init方法里面返回
		 * @param {Object} appId 腾讯云appid
		 * @param {Object} roomId 房间号
		 * @param {Object} appScene  代表使用场景，值为0：视频通话、1：视频互动直播、2：语音通话、3：语音互动直播
		 * @param {Object} userId
		 * @param {Object} userSig
		 * @param {Object} callback
		 */
		enterRoom(data, callback) {
			ux_jyrtc.jy_enterRoom(data, res => {
				callback && callback(res);
			})
		},
		/**
		 * @description  退出房间，房间相关处理结果均在init方法里面返回
		 * @param {Object} callback
		 */
		exitRoom(callback) {
			ux_jyrtc.jy_enterRoom(res => {
				callback && callback(res);
			})
		},
		/**
		 * @description 设置本地图像的渲染设置
		 * @param {Object} fillMode "0：图像铺满屏蔽，超过视窗的被剪裁；1：图像长边填满屏幕，短边会是黑色",
		 * @param {Object} mirrorType "0：前置摄像头镜像、后置不镜像；1：前后均镜像；2前后均不镜像",
		 * @param {Object} rotation "0：不旋转；1：旋转90度；2：旋转180；3：旋转270"
		 */
		setLocalRenderParams(fillMode, mirrorType, rotation, callback) {
			ux_jyrtc.jy_enterRoom({
					fillMode: fillMode,
					mirrorType: mirrorType,
					rotation: rotation
				},
				res => {
					callback && callback(res);
				});
		},

		/**
		 * @description  停止本地音频预览
		 * @param {Object} callback
		 */
		stopLocalAudio(callback) {
			ux_jyrtc.jy_stopLocalAudio(res => {
				callback && callback(res);
			})
		},

		/**
		 * @description 静音所有用户的音频
		 * @param {Object} mute "1是静音，0是取消静音"
		 * @param {Object} callback
		 */
		muteAllRemoteAudio(mute, callback) {
			ux_jyrtc.jy_muteAllRemoteAudio({
				mute: mute
			}, res => {
				callback && callback(res)
			});
		},
	},

	/**
	 * @description  投屏相关
	 */
	lyzmlDLNA: {
		/**
		 * @description 搜索设备
		 * @param {Object} callback
		 */
		startSearch(callback) {
			ux_lyzmlDLNA.startSearch(res => {
				if (callback) callback(res);
			})
		},
		/**
		 * @description 停止搜索
		 * @param {Object} callback
		 */
		stopSearch(callback) {
			ux_lyzmlDLNA.stopSearch(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 获取搜索结果
		 * @param {Object} callback
		 */
		getSearchResult(callback) {
			ux_lyzmlDLNA.getSearchResult(res => {
				if (callback) callback(res);
			});
		},
		/**
		 * @description 播放内容
		 * @param {Object} ip
		 * @param {Object} url
		 */
		playVideo(ip, url) {
			var data = {
				ip: ip,
				mediaURL: url
			};
			ux_lyzmlDLNA.playVideo(data, res => {
				if (callback) callback(res);
			});
		},
		/**
		 * 停止播放
		 * @param {Object} callback
		 */
		stopVideo(callback) {
			ux_lyzmlDLNA.stopVideo(data, res => {
				if (callback) callback(res);
			});
		},
		getIpAddress(callback) {
			ux_lyzmlDLNA.getIpAddress(res => {
				if (callback) callback(res);
			});
		},
	},
	/**
	 * 相机相关
	 */
	camera: {
		/**
		 * @description  开启相机
		 */
		startCamera() {
			ux_camera.startCamera();
		},
	},
	/**
	 * @description  用户信息
	 */
	login: {
		/**
		 * @description 是否已经登录
		 * @param {Object} callback
		 */
		isLogin(callback){
			lobsterapp.sendEvent("islogin",{},res=>{
				callback && callback(res)
			})
		},
		/**
		 * @description  获取登录信息
		 * @param {Object} callback
		 */
		getuserInfo(callback){
			lobsterapp.sendEvent("logindata",{},res=>{
				callback && callback(res)
			})
		},
	},
	event: {
		/**
		 * @param {Object} event 事件名称
		 * @param {Object} data 事件参数
		 * @param {Object} callback 回调
		 */
		sendEvent(event, data, callback) {
			// 向宿主App发送事件
			uni.sendNativeEvent(event, data, res => {
				console.log('宿主App回传的数据：' + res);
				callback && callback(res)
			})
		},
	}
}
export default lobsterapp;
