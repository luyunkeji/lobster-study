const tim = uni.requireNativePlugin("ux-tim");
/**
 * 本模块封装了IM相关的一些方法
 */
const imCommon = {
	//视频消息对象
	videoInfo: {
		"localUrl": '', //本地路径  用于重新发送的时候
		"status": 1, //1消息发送中 2消息发送成功 3消息发送失败(服务端失败)  4消息被删除  5导入到本地的消息 6被撤销的消息 7 本地发送失败的消息（重新发送后不显示)  10消息发送失败（用于本地重新发送）
		"offlinePushInfo": {
			"disablePush": false
		},
		"cloudCustomData": "", //云端自定义数据（云端保存，会发送到对端，程序卸载重装后还能拉取到）
		"self": false, //消息发送者是否是自己
		"localCustomData": "", //消息自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"elemType": 5, //消息类型  0没有元素  1文本消息  2自定义消息  3图片消息 4语音消息  5视频消息 6文件消息 7地理位置消息 8表情消息 9群 Tips 消息（存消息列表）
		"localCustomInt": 0, //自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"userID": "", //消息接受者
		"groupAtUserList": [], //群消息中被 @ 的用户 UserID 列表（即该消息都 @ 了哪些人）
		"msgID": "", //消息 ID 唯一标识
		"sender": "", //发送者 userID
		"random": 0, //消息随机码
		"faceUrl": "", //头像 url
		"timestamp": 0, //消息时间戳
		"priority": 2, //优先级
		"read": false, //是否已读
		"peerRead": false, //对方是否已读（只有 C2C 消息有效）
		"seq": 0, //消息的序列号
		"nickName": "", //消息发送者昵称
		"videoElem": {
			"snapshotSize": 0, //截图大小
			"videoUUID": "", //视频文件 ID
			"videoSize": 0, //视频文件大小
			"snapshotPath": "", //设置的截图路径
			"snapshotWidth": 0, //截图宽度
			"videoPath": "", //视频文件路径（只有发送方才能获取到）
			"snapshotHeight": 0, //截图高度
			"snapshotUUID": "", //截图 ID，内部标识，可用于外部缓存 key
			"duration": 0 //视频时长，单位：秒
		},
		"height": 0, //短视频显示的高度（自定义的属性，非腾讯云对象属性)
		"width": 0, //短视频显示的宽度（自定义的属性，非腾讯云对象属性)
		"sending": false, //发送中
		"groupID":"",//群编号
	},
	//文本消息对象
	textInfo: {
		"status": 1, //1消息发送中 2消息发送成功 3消息发送失败(服务端失败)  4消息被删除  5导入到本地的消息 6被撤销的消息 7 本地发送失败的消息（重新发送后不显示)  10消息发送失败（用于本地重新发送）
		"offlinePushInfo": {
			"disablePush": false
		},
		"cloudCustomData": "", //云端自定义数据（云端保存，会发送到对端，程序卸载重装后还能拉取到）
		"self": false, //消息发送者是否是自己
		"localCustomData": "", //消息自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"elemType": 1, //消息类型  0没有元素  1文本消息  2自定义消息  3图片消息 4语音消息  5视频消息 6文件消息 7地理位置消息 8表情消息 9群 Tips 消息（存消息列表）
		"localCustomInt": 0, //自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"userID": "", //消息接受者
		"groupAtUserList": [], //群消息中被 @ 的用户 UserID 列表（即该消息都 @ 了哪些人）
		"msgID": "", //消息 ID 唯一标识
		"sender": "", //发送者 userID
		"random": 0, //消息随机码
		"faceUrl": "", //头像 url
		"timestamp": 0, //消息时间戳
		"priority": 2, //优先级
		"read": false, //是否已读
		"peerRead": false, //对方是否已读（只有 C2C 消息有效）
		"seq": 0, //消息的序列号
		"nickName": "", //消息发送者昵称
		"textElem": {
			"text": "" //消息文本
		},
		"sending": false, //发送中
		"groupID":"",//群编号
	},
	//图片属性
	imgInfo: {
		"localUrl": "", //临时存储本地图片路径 用于发送失败后重新发送
		"status": 1, //1消息发送中 2消息发送成功 3消息发送失败(服务端失败)  4消息被删除  5导入到本地的消息 6被撤销的消息 7 本地发送失败的消息（重新发送后不显示)  10消息发送失败（用于本地重新发送）
		"offlinePushInfo": {
			"disablePush": false
		},
		"cloudCustomData": "", //云端自定义数据（云端保存，会发送到对端，程序卸载重装后还能拉取到）
		"self": false, //消息发送者是否是自己
		"localCustomData": "", //消息自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"elemType": 3, //消息类型  0没有元素  1文本消息  2自定义消息  3图片消息 4语音消息  5视频消息 6文件消息 7地理位置消息 8表情消息 9群 Tips 消息（存消息列表）
		"localCustomInt": 0, //自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"userID": "", //消息接受者
		"groupAtUserList": [], //群消息中被 @ 的用户 UserID 列表（即该消息都 @ 了哪些人）
		"msgID": "", //消息 ID 唯一标识
		"sender": "", //发送者 userID
		"random": 0, //消息随机码
		"faceUrl": "", //头像 url
		"timestamp": 0, //消息时间戳
		"priority": 2, //优先级
		"read": false, //是否已读
		"peerRead": false, //对方是否已读（只有 C2C 消息有效）
		"seq": 0, //消息的序列号
		"nickName": "", //消息发送者昵称
		"imageElem": {
			"imageList": [{
					"height": 0, //图片高度
					"url": "", //图片url
					"uUID": "", //唯一标示用于缓存的key
					"type": 0, //图片类型  0原图  1缩略图  2大图
					"width": 0, //图片宽度
					"size": 0, //图片大小
				},
				{
					"height": 0, //图片高度
					"url": "", //图片url
					"uUID": "", //唯一标示用于缓存的key
					"type": 1, //图片类型  0原图  1缩略图  2大图
					"width": 0, //图片宽度
					"size": 0, //图片大小
				},
				{
					"height": 0, //图片高度
					"url": "", //图片url
					"uUID": "", //唯一标示用于缓存的key
					"type": 2, //图片类型  0原图  1缩略图  2大图
					"width": 0, //图片宽度
					"size": 0, ////图片大小
				}
			],
			"path": "" //原图本地文件路径，只对消息发送方有效
		},
		"height": 0, //短视频显示的高度（自定义的属性，非腾讯云对象属性)
		"width": 0, //短视频显示的宽度（自定义的属性，非腾讯云对象属性)
		"sending": false, //发送中
		"groupID":"",//群编号
	},
	//语音消息
	voiceInfo: {
		"localUrl": '', //本地路径  用于重新发送
		"status": 1, //1消息发送中 2消息发送成功 3消息发送失败(服务端失败)  4消息被删除  5导入到本地的消息 6被撤销的消息 7 本地发送失败的消息（重新发送后不显示)  10消息发送失败（用于本地重新发送）
		"offlinePushInfo": {
			"disablePush": false
		},
		"cloudCustomData": "", //云端自定义数据（云端保存，会发送到对端，程序卸载重装后还能拉取到）
		"self": false, //消息发送者是否是自己
		"localCustomData": "", //消息自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"elemType": 4, //消息类型  0没有元素  1文本消息  2自定义消息  3图片消息 4语音消息  5视频消息 6文件消息 7地理位置消息 8表情消息 9群 Tips 消息（存消息列表）
		"localCustomInt": 0, //自定义数据（本地保存，不会发送到对端，程序卸载重装后失效）
		"userID": "", //消息接受者
		"groupAtUserList": [], //群消息中被 @ 的用户 UserID 列表（即该消息都 @ 了哪些人）
		"msgID": "", //消息 ID 唯一标识
		"sender": "", //发送者 userID
		"random": 0, //消息随机码
		"faceUrl": "", //头像 url
		"timestamp": 0, //消息时间戳
		"priority": 2, //优先级
		"read": false, //是否已读
		"peerRead": false, //对方是否已读（只有 C2C 消息有效）
		"seq": 0, //消息的序列号
		"nickName": "", //消息发送者昵称
		"soundElem": {
			"uUID": "", //uuid
			"path": "", //路径
			"dataSize": 0, //文件大小
			"duration": 0 //时长，单位：秒
		},
		"width": 0, //语音条长度  （自定义的属性，非腾讯云对象属性)
		"sending": false, //发送中
		"groupID":"",//群编号
	},
	//信令消息返回
	signalingInfo: {
		"timeout": 0,
		"inviter": "",
		"businessID": 1,
		"data": "",
		"inviteID": "",
		"onlineUserOnly": false,
		"groupID": "",
		"inviteeList": [],
		"actionType": 0, //1,邀请方发起邀请 2,邀请方取消邀请 3,被邀请方接受邀请 4,被邀请方拒绝邀请, 5邀请超时
	},
	//表情对象
	onlineEmoji: {
		"100.gif": "AbNQgA.gif",
		"101.gif": "AbN3ut.gif",
		"102.gif": "AbNM3d.gif",
		"103.gif": "AbN8DP.gif",
		"104.gif": "AbNljI.gif",
		"105.gif": "AbNtUS.gif",
		"106.gif": "AbNGHf.gif",
		"107.gif": "AbNYE8.gif",
		"108.gif": "AbNaCQ.gif",
		"109.gif": "AbNN4g.gif",
		"110.gif": "AbN0vn.gif",
		"111.gif": "AbNd3j.gif",
		"112.gif": "AbNsbV.gif",
		"113.gif": "AbNwgs.gif",
		"114.gif": "AbNrD0.gif",
		"115.gif": "AbNDuq.gif",
		"116.gif": "AbNg5F.gif",
		"117.gif": "AbN6ET.gif",
		"118.gif": "AbNcUU.gif",
		"119.gif": "AbNRC4.gif",
		"120.gif": "AbNhvR.gif",
		"121.gif": "AbNf29.gif",
		"122.gif": "AbNW8J.gif",
		"123.gif": "AbNob6.gif",
		"124.gif": "AbN5K1.gif",
		"125.gif": "AbNHUO.gif",
		"126.gif": "AbNIDx.gif",
		"127.gif": "AbN7VK.gif",
		"128.gif": "AbNb5D.gif",
		"129.gif": "AbNX2d.gif",
		"130.gif": "AbNLPe.gif",
		"131.gif": "AbNjxA.gif",
		"132.gif": "AbNO8H.gif",
		"133.gif": "AbNxKI.gif",
		"134.gif": "AbNzrt.gif",
		"135.gif": "AbU9Vf.gif",
		"136.gif": "AbUSqP.gif",
		"137.gif": "AbUCa8.gif",
		"138.gif": "AbUkGQ.gif",
		"139.gif": "AbUFPg.gif",
		"140.gif": "AbUPIS.gif",
		"141.gif": "AbUZMn.gif",
		"142.gif": "AbUExs.gif",
		"143.gif": "AbUA2j.gif",
		"144.gif": "AbUMIU.gif",
		"145.gif": "AbUerq.gif",
		"146.gif": "AbUKaT.gif",
		"147.gif": "AbUmq0.gif",
		"148.gif": "AbUuZV.gif",
		"149.gif": "AbUliF.gif",
		"150.gif": "AbU1G4.gif",
		"151.gif": "AbU8z9.gif",
		"152.gif": "AbU3RJ.gif",
		"153.gif": "AbUYs1.gif",
		"154.gif": "AbUJMR.gif",
		"155.gif": "AbUadK.gif",
		"156.gif": "AbUtqx.gif",
		"157.gif": "AbUUZ6.gif",
		"158.gif": "AbUBJe.gif",
		"159.gif": "AbUdIO.gif",
		"160.gif": "AbU0iD.gif",
		"161.gif": "AbUrzd.gif",
		"162.gif": "AbUDRH.gif",
		"163.gif": "AbUyQA.gif",
		"164.gif": "AbUWo8.gif",
		"165.gif": "AbU6sI.gif",
		"166.gif": "AbU2eP.gif",
		"167.gif": "AbUcLt.gif",
		"168.gif": "AbU4Jg.gif",
		"169.gif": "AbURdf.gif",
		"170.gif": "AbUhFS.gif",
		"171.gif": "AbU5WQ.gif",
		"172.gif": "AbULwV.gif",
		"173.gif": "AbUIzj.gif",
		"174.gif": "AbUTQs.gif",
		"175.gif": "AbU7yn.gif",
		"176.gif": "AbUqe0.gif",
		"177.gif": "AbUHLq.gif",
		"178.gif": "AbUOoT.gif",
		"179.gif": "AbUvYF.gif",
		"180.gif": "AbUjFU.gif",
		"181.gif": "AbaSSJ.gif",
		"182.gif": "AbUxW4.gif",
		"183.gif": "AbaCO1.gif",
		"184.gif": "Abapl9.gif",
		"185.gif": "Aba9yR.gif",
		"186.gif": "AbaFw6.gif",
		"187.gif": "Abaiex.gif",
		"188.gif": "AbakTK.gif",
		"189.gif": "AbaZfe.png",
		"190.gif": "AbaEFO.gif",
		"191.gif": "AbaVYD.gif",
		"192.gif": "AbamSH.gif",
		"193.gif": "AbaKOI.gif",
		"194.gif": "Abanld.gif",
		"195.gif": "Abau6A.gif",
		"196.gif": "AbaQmt.gif",
		"197.gif": "Abal0P.gif",
		"198.gif": "AbatpQ.gif",
		"199.gif": "Aba1Tf.gif",
		"200.png": "Aba8k8.png",
		"201.png": "AbaGtS.png",
		"202.png": "AbaJfg.png",
		"203.png": "AbaNlj.png",
		"204.png": "Abawmq.png",
		"205.png": "AbaU6s.png",
		"206.png": "AbaaXn.png",
		"207.png": "Aba000.png",
		"208.png": "AbarkT.png",
		"209.png": "AbastU.png",
		"210.png": "AbaB7V.png",
		"211.png": "Abafn1.png",
		"212.png": "Abacp4.png",
		"213.png": "AbayhF.png",
		"214.png": "Abag1J.png",
		"215.png": "Aba2c9.png",
		"216.png": "AbaRXR.png",
		"217.png": "Aba476.png",
		"218.png": "Abah0x.png",
		"219.png": "Abdg58.png"
	},

	//表情
	emojiList: [
		[{
			"url": "100.gif",
			alt: "[微笑]"
		}, {
			"url": "101.gif",
			alt: "[伤心]"
		}, {
			"url": "102.gif",
			alt: "[美女]"
		}, {
			"url": "103.gif",
			alt: "[发呆]"
		}, {
			"url": "104.gif",
			alt: "[墨镜]"
		}, {
			"url": "105.gif",
			alt: "[哭]"
		}, {
			"url": "106.gif",
			alt: "[羞]"
		}, {
			"url": "107.gif",
			alt: "[哑]"
		}, {
			"url": "108.gif",
			alt: "[睡]"
		}, {
			"url": "109.gif",
			alt: "[哭]"
		}, {
			"url": "110.gif",
			alt: "[囧]"
		}, {
			"url": "111.gif",
			alt: "[怒]"
		}, {
			"url": "112.gif",
			alt: "[调皮]"
		}, {
			"url": "113.gif",
			alt: "[笑]"
		}, {
			"url": "114.gif",
			alt: "[惊讶]"
		}, {
			"url": "115.gif",
			alt: "[难过]"
		}, {
			"url": "116.gif",
			alt: "[酷]"
		}, {
			"url": "117.gif",
			alt: "[汗]"
		}, {
			"url": "118.gif",
			alt: "[抓狂]"
		}, {
			"url": "119.gif",
			alt: "[吐]"
		}, {
			"url": "120.gif",
			alt: "[笑]"
		}, {
			"url": "121.gif",
			alt: "[快乐]"
		}, {
			"url": "122.gif",
			alt: "[奇]"
		}, {
			"url": "123.gif",
			alt: "[傲]"
		}],
		[{
			"url": "124.gif",
			alt: "[饿]"
		}, {
			"url": "125.gif",
			alt: "[累]"
		}, {
			"url": "126.gif",
			alt: "[吓]"
		}, {
			"url": "127.gif",
			alt: "[汗]"
		}, {
			"url": "128.gif",
			alt: "[高兴]"
		}, {
			"url": "129.gif",
			alt: "[闲]"
		}, {
			"url": "130.gif",
			alt: "[努力]"
		}, {
			"url": "131.gif",
			alt: "[骂]"
		}, {
			"url": "132.gif",
			alt: "[疑问]"
		}, {
			"url": "133.gif",
			alt: "[秘密]"
		}, {
			"url": "134.gif",
			alt: "[乱]"
		}, {
			"url": "135.gif",
			alt: "[疯]"
		}, {
			"url": "136.gif",
			alt: "[哀]"
		}, {
			"url": "137.gif",
			alt: "[鬼]"
		}, {
			"url": "138.gif",
			alt: "[打击]"
		}, {
			"url": "139.gif",
			alt: "[bye]"
		}, {
			"url": "140.gif",
			alt: "[汗]"
		}, {
			"url": "141.gif",
			alt: "[抠]"
		}, {
			"url": "142.gif",
			alt: "[鼓掌]"
		}, {
			"url": "143.gif",
			alt: "[糟糕]"
		}, {
			"url": "144.gif",
			alt: "[恶搞]"
		}, {
			"url": "145.gif",
			alt: "[什么]"
		}, {
			"url": "146.gif",
			alt: "[什么]"
		}, {
			"url": "147.gif",
			alt: "[累]"
		}],
		[{
			"url": "148.gif",
			alt: "[看]"
		}, {
			"url": "149.gif",
			alt: "[难过]"
		}, {
			"url": "150.gif",
			alt: "[难过]"
		}, {
			"url": "151.gif",
			alt: "[坏]"
		}, {
			"url": "152.gif",
			alt: "[亲]"
		}, {
			"url": "153.gif",
			alt: "[吓]"
		}, {
			"url": "154.gif",
			alt: "[可怜]"
		}, {
			"url": "155.gif",
			alt: "[刀]"
		}, {
			"url": "156.gif",
			alt: "[水果]"
		}, {
			"url": "157.gif",
			alt: "[酒]"
		}, {
			"url": "158.gif",
			alt: "[篮球]"
		}, {
			"url": "159.gif",
			alt: "[乒乓]"
		}, {
			"url": "160.gif",
			alt: "[咖啡]"
		}, {
			"url": "161.gif",
			alt: "[美食]"
		}, {
			"url": "162.gif",
			alt: "[动物]"
		}, {
			"url": "163.gif",
			alt: "[鲜花]"
		}, {
			"url": "164.gif",
			alt: "[枯]"
		}, {
			"url": "165.gif",
			alt: "[唇]"
		}, {
			"url": "166.gif",
			alt: "[爱]"
		}, {
			"url": "167.gif",
			alt: "[分手]"
		}, {
			"url": "168.gif",
			alt: "[生日]"
		}, {
			"url": "169.gif",
			alt: "[电]"
		}, {
			"url": "170.gif",
			alt: "[炸弹]"
		}, {
			"url": "171.gif",
			alt: "[刀子]"
		}],
		[{
			"url": "172.gif",
			alt: "[足球]"
		}, {
			"url": "173.gif",
			alt: "[瓢虫]"
		}, {
			"url": "174.gif",
			alt: "[翔]"
		}, {
			"url": "175.gif",
			alt: "[月亮]"
		}, {
			"url": "176.gif",
			alt: "[太阳]"
		}, {
			"url": "177.gif",
			alt: "[礼物]"
		}, {
			"url": "178.gif",
			alt: "[抱抱]"
		}, {
			"url": "179.gif",
			alt: "[拇指]"
		}, {
			"url": "180.gif",
			alt: "[贬低]"
		}, {
			"url": "181.gif",
			alt: "[握手]"
		}, {
			"url": "182.gif",
			alt: "[剪刀手]"
		}, {
			"url": "183.gif",
			alt: "[抱拳]"
		}, {
			"url": "184.gif",
			alt: "[勾引]"
		}, {
			"url": "185.gif",
			alt: "[拳头]"
		}, {
			"url": "186.gif",
			alt: "[小拇指]"
		}, {
			"url": "187.gif",
			alt: "[拇指八]"
		}, {
			"url": "188.gif",
			alt: "[食指]"
		}, {
			"url": "189.gif",
			alt: "[ok]"
		}, {
			"url": "190.gif",
			alt: "[情侣]"
		}, {
			"url": "191.gif",
			alt: "[爱心]"
		}, {
			"url": "192.gif",
			alt: "[蹦哒]"
		}, {
			"url": "193.gif",
			alt: "[颤抖]"
		}, {
			"url": "194.gif",
			alt: "[怄气]"
		}, {
			"url": "195.gif",
			alt: "[跳舞]"
		}],
		[{
			"url": "196.gif",
			alt: "[发呆]"
		}, {
			"url": "197.gif",
			alt: "[背着]"
		}, {
			"url": "198.gif",
			alt: "[伸手]"
		}, {
			"url": "199.gif",
			alt: "[耍帅]"
		}, {
			"url": "200.png",
			alt: "[微笑]"
		}, {
			"url": "201.png",
			alt: "[生病]"
		}, {
			"url": "202.png",
			alt: "[哭泣]"
		}, {
			"url": "203.png",
			alt: "[吐舌]"
		}, {
			"url": "204.png",
			alt: "[迷糊]"
		}, {
			"url": "205.png",
			alt: "[瞪眼]"
		}, {
			"url": "206.png",
			alt: "[恐怖]"
		}, {
			"url": "207.png",
			alt: "[忧愁]"
		}, {
			"url": "208.png",
			alt: "[眨眉]"
		}, {
			"url": "209.png",
			alt: "[闭眼]"
		}, {
			"url": "210.png",
			alt: "[鄙视]"
		}, {
			"url": "211.png",
			alt: "[阴暗]"
		}, {
			"url": "212.png",
			alt: "[小鬼]"
		}, {
			"url": "213.png",
			alt: "[礼物]"
		}, {
			"url": "214.png",
			alt: "[拜佛]"
		}, {
			"url": "215.png",
			alt: "[力量]"
		}, {
			"url": "216.png",
			alt: "[金钱]"
		}, {
			"url": "217.png",
			alt: "[蛋糕]"
		}, {
			"url": "218.png",
			alt: "[彩带]"
		}, {
			"url": "219.png",
			alt: "[礼物]"
		}, ]
	],

	/**
	 * 初始化
	 * @sdkAppID 腾讯云sdk
	 * @callback 回调
	 */
	initSDK(sdkAppID, callback) {
		tim.initSDK(parseInt(sdkAppID), res => {
			if (callback) {
				callback(res);
			}
		})
	},
	//
	//callback
	/**
	 * 登录
	 * userId 用户id
	 * userSig 用户标识
	 * @callback 回调
	 */
	login(userId, userSig, callback) {
		tim.login(userId, userSig, res => {
			//设置离线推送
			// tim.setOfflinePushConfig(re=>{
			// 	if (callback)
			// 		callback(re);
			// })
			if (callback)
				callback(res);
		})
	},
	/**
	 * 发送图片消息
	 * @data  {userID imgPath busnessId} 图片信息{对方userid、图片路径、业务编号id}
	 * @callback 回调
	 */
	sendC2CImgMessage(groupId,imagePath, userID, cloudCustomData, callback) {
		tim.sendC2CImageMessage(groupId,imagePath, userID, cloudCustomData, res => {
			if (callback) {
				callback(res);
			}
		})
	},

	/**
	 * 发送文本消息
	 * @data  {userID text busnessId} 文本消息信息{对方userid、文本内容、业务编号id}
	 * @callback 回调
	 */
	sendC2CTextMessage(groupId,text, userID, cloudCustomData, callback) {
		tim.sendC2CTextMessage(groupId,text, userID, cloudCustomData, res => {
			console.log(res)
			if (callback) callback(res);
		})
	},

	/**
	 * 发送语音消息
	 * @data  {soundPath duration  userID busnessId} 音频地址、时长、对方用户id、 业务编号id
	 * @callback 回调
	 */
	sendSoundMessage(groupId,soundPath, duration, userID, cloudCustomData, callback) {
		tim.sendSoundMessage(groupId,soundPath, duration, userID, cloudCustomData, result => {
			if (callback) callback(result);
		});
	},

	/**
	 * 发送短视频消息
	 * @data  {videoFilePath duration  userID type busnessId} 短视频地址、时长、对方用户id、类型、 业务编号id
	 * @callback 回调
	 */
	sendVideoMessage(groupId,videoFilePath, duration, type, snapshotPath, userID, cloudCustomData, callback, proback) {
		//console.log(data);
		tim.sendVideoMessage(groupId,videoFilePath, duration, type, snapshotPath, userID, cloudCustomData, result => {
			callback(result);
		}, pro => {
			proback(pro);
		})
	},
	/**
	 *  获取信令信息
	 */
	getSignalingInfo(msgId, callback) {
		tim.getSignalingInfo(msgId, res => {
			//console.log(res);
			if (callback) callback(res);
		});
	},
	//自定义消息
	getCustomMessageData2String(msgId,callback){
		tim.getCustomMessageData2String(msgId, res => {
			//console.log(res);
			if (callback) callback(res);
		});
	},

	/**
	 * 邀请视频通话
	 */
	invite(userId, callback) {
		tim.invite(userId, '2049', false, 60, res => {
			callback(res);
		});
	},

	/**
	 * 获取短视频封面
	 * @callback 回调
	 */
	getVideoThumbnail(url) {
		return new Promise((resolve, reject) => {
			try {
				console.log("准备获取封面");
				tim.GetVideoThumbnail(url, res => {
					console.log("准备获取封面准备返回");
					console.log(res);
					if (res.code == 0) {
						resolve(res);
					} else {
						reject(res)
					}
				})
			} catch (res) {
				console.log(res);
				uni.showToast({
					title: JSON.stringify(res)
				})
				reject(res);
			}
		});
	},
	saveFile(url) {
		return new Promise((resolve, reject) => {
			try {
				uni.saveFile({
					tempFilePath: url,
					success: function(res) {
						var savedFilePath = res.savedFilePath;
						resolve(savedFilePath)
					},
					fail: function(res) {
						// uni.showModal({
						// 	 title :"saveFileFail",
						// 	 content:JSON.stringify(res)
						// });
						console.log(res);
						resolve("");
					}
				});
			} catch (res) {
				uni.showModal({
					title: "saveFileError",
					content: JSON.stringify(res)
				});
				console.log(res);
				reject(res);
			}
		});

	},


	/**
	 * 事件监听
	 * @callback 回调
	 */
	addAdvancedMsgListener(callback) {
		tim.addAdvancedMsgListener(res => {
			if (callback) callback(res);
		})
	},

	/**
	 * 信令监听
	 * @callback 回调
	 */
	addSignalingListener(callback) {
		tim.addSignalingListener(res => {
			if (callback) callback(res);
		})
	},
	/**
	 * 移除监听
	 * @callback 回调
	 */
	removeSignalingListener(callback) {
		tim.removeSignalingListener(res => {
			if (callback) callback(res);
		})
	},
	/**
	 * 邀请
	 * @callback 回调
	 * @invite 	被邀请人用户 ID
	 * @data 自定义数据
	 * @onlineUserOnly 是否只有在线用户才能收到邀请，如果设置为 true，
	 * 只有在线用户才能收到， 并且 invite 操作也不会产生历史消息
	 * （针对该次 invite 的后续 cancel、accept、reject、timeout 操作也同样不会产生历史消息）
	 * @timeout 超时时间，单位秒，如果设置为 0，SDK 不会做超时检测，
	 * 也不会触发 onInvitationTimeout 回调
	 */
	invite(invitee, data, onlineUserOnly, timeout, callback) {
		tim.invite(invitee, data, onlineUserOnly, timeout, res => {
			if (callback) callback(res);
		})
	},
	/**
	 * 邀请方取消邀请
	 * 如果所有被邀请人都已经处理了当前邀请（包含超时），不能再取消当前邀请。
	 *
	 * @param inviteId  	邀请 ID  发送邀请后返回编号
	 * @param data 自定义数据
	 * @param callback
	 */
	cancelInvite(inviteId, callback) {
		tim.cancelInvite(inviteId, res => {
			if (callback) callback(res);
		})
	},
	/**
	 * 接收方接收邀请
	 * 不能接受不是针对自己的邀请，
	 * 请在收到 onReceiveNewInvitation 回调的时候先判断 inviteeList 有没有自己，
	 * 如果没有自己，不能 accept 邀请。
	 *
	 * @param inviteId
	 * @param data
	 * @param callback
	 */
	acceptInvite(inviteId, data, callback) {
		tim.acceptInvite(inviteId, data, res => {
			if (callback) callback(res);
		})
	},

	/**
	 * 接收方拒绝邀请
	 * 不能拒绝不是针对自己的邀请，
	 * 请在收到 onReceiveNewInvitation 回调的时候先判断 
	 * inviteeList 有没有自己，如果没有自己，不能 reject 邀请。
	 * @param inviteId
	 * @param data
	 * @param callback
	 */
	rejectInvite(inviteId, data, callback) {
		tim.rejectInvite(inviteId, data, res => {
			if (callback) callback(res);
		})
	},

	/**
	 * 获取历史数据
	 * @touserId 对方用户编号
	 * @count 数量
	 * @lastMsgId 最后一条数据对象
	 * @callback 回调
	 */
	getC2CHistoryMessageList(touserId, count, lastMsgId, callback) {
		console.log(touserId + '_' + count + '_' + lastMsgId);
		try {
			tim.getC2CHistoryMessageList(touserId, count, lastMsgId, res => {
				if (callback) callback(res);
			})
		} catch (res) {
			console.log(res);
			uni.showToast({
				title: JSON.stringify(res)
			})
		}
	},

	/**
	 * 移除监听
	 */
	removeAdvancedMsgListener() {
		tim.removeAdvancedMsgListener();
	},
	/**
	 * 获取会话列表
	 */
	getConversationList(nextSeq, count, callback) {
		tim.getConversationList(nextSeq, count, res => {
			if (callback) callback(res);
		});
	},
	/**
	 * 设置会话监听
	 */
	setConversationListener(callback) {
		tim.setConversationListener(res => {
			if (callback) callback(res);
		})
	},
	/**
	 * 删除会话
	 */
	deleteConversation(conversationID, callback) {
		tim.deleteConversation(conversationID, res => {
			if (callback) callback(res);
		});
	},
	/**
	 * 获取会话未读总数
	 */
	getTotalUnreadMessageCount(callback) {
		tim.getTotalUnreadMessageCount(res=>{
			if(callback)callback(res);
		})
	},
	/**
	 * 设置消息已读
	 */
	markC2CMessageAsRead(userID,callback) {
		tim.markC2CMessageAsRead(userID,res=>{
			if(callback)callback(res);
		})
	},
  	/**
	 * 设置消息置顶
	 */
	pinConversation(conversationID,isPinned,callback){
		tim.pinConversation(conversationID,isPinned,res=>{
			if(callback)callback(res);
		})
	},
	/**
	 * 退出登录
	 * @callback 回调
	 */
	logout(callback) {
		tim.logout(res => {
			if (callback) callback(res);
		});
	},
	/**
	 * 获取登录状态
	 */
	getLoginStatus(){
		return tim.getLoginStatus();
	},
	/**
	 * 获取当前登录用户
	 */
	getLoginUser(){
		return tim.getLoginUser();
	},
	/**
	 * 卸载sdk
	 * @callback 回调
	 */
	unInitSDK(callback) {
		tim.unInitSDK(res => {
			if (callback) callback();
		});
	},

	/**
	 * 下载图片
	 * @callback 调用回调
	 * @downCallback 下载回调
	 */
	downloadImage(msgId, callback, downCallback) {
		tim.downloadImage(msgId,
			back => {
				if (callback) callback(back);
			}, downback => {
				if (downCallback) downCallback(downback);
			});
	},
	/**
	 * 下载语音
	 * @callback 调用回调
	 * @downCallback 下载回调
	 */
	downloadSound(msgId, callback, downCallback) {
		tim.downloadSound(msgId,
			back => {
				if (callback) callback(back);
			}, downback => {
				if (downCallback) downCallback(downback);
			});
	},

	/**
	 * 下载短视频
	 * @callback 调用回调
	 * @downCallback 下载回调
	 */
	downloadVideo(msgId, callback, downCallback) {
		tim.downloadVideo(msgId,
			back => {
				if (callback) callback(back);
			}, downback => {
				if (downCallback) downCallback(downback);
			});
	},

	/**
	 * 下载短视频封面
	 * @callback 调用回调
	 * @downCallback 下载回调
	 */
	downloadVideoImg(msgId, callback, downCallback) {
		tim.downloadVideoImg(msgId,
			back => {
				if (callback) callback(back);
			}, downback => {
				if (downCallback) downCallback(downback);
			});
	},

	/**
	 * 撤回消息
	 * @callback 调用回调
	 */
	revokeMessage(msgId, callback) {
		tim.revokeMessage(msgId, res => {
			if (callback) callback(res);
		})
	},

	/**
	 * 生成本地的一个唯一标识
	 */
	localCustomData(userId, touserId, busnessId) {
  	return userId + '_' + touserId + '_' + busnessId + '_' + new Date().getTime();
	},

	/**
	 * 获取图片信息
	 * @callback 回调
	 */
	getImageInfo(url) {
		return new Promise((resolve, reject) => {
			try {
				uni.getImageInfo({
					src: url,
					success: res => {
						resolve(res);
					},
					fail: res => {
						reject(res);
					}
				})
			} catch (res) {
				reject(res);
			}
		});
	},



	/**@dateTimeFliter 转换格林日期时间格式为常用日期格式
	 * @time[必填] 						Date  		格林日期格式
	 * @part[可选,默认:0]				Number      选择返回日期时间部分  列:0:返回所有 1:只返回日期  2:只返回时间
	 * @dateComplete[可选,默认:true] 	Boolean 	日期位数不足是否添0补齐:true:补齐,false:不补齐
	 * @timeComplete[可选,默认:true] 	Boolean 	时间位数不足是否添0补齐:true:补齐,false:不补齐
	 * @dateConnector[可选,默认:-] 		String 		年月日连接符  例: - : /
	 * @timeConnector[可选,默认::] 		String 		时间连接符   例: - : /
	 * @hour12[可选,默认:false]          Boolean     是否返回12小时制时间   例: true:返回12小时制时间   false:返回24小时制时间
	 * @return   '2019-11-25 15:05:54'  String    返回示例
	 * **/
	dateTimeFliter(time, part = 0, dateComplete = true, timeComplete = true, dateConnector = '-', timeConnector =
		':', hour12 = false) {
		// 	console.log(time);
		// time = time.replace(/-/g, '/')
		// console.log(time);
		//console.log(time)
		let year = time.getFullYear()
		let month = time.getMonth() + 1
		let day = time.getDate()
		let hour = time.getHours()
		let minute = time.getMinutes()
		let second = time.getSeconds()
		let dateStr = ''
		let timeStr = ''
		//转换日期
		if (dateComplete) { //添0补齐
			if (month < 10) {
				month = '0' + month
			}
			if (day < 10) {
				day = '0' + day
			}
		}

		//转换时间
		//修改小时制
		if (hour12) {
			if (hour > 12) {
				hour = hour - 12
				if (timeComplete) {
					if (hour < 10) {
						hour = '下午 ' + '0' + hour
					} else {
						hour = '下午 ' + hour
					}
				}
			} else {
				if (timeComplete) {
					if (hour < 10) {
						hour = '上午 ' + '0' + hour
					} else {
						hour = '上午 ' + hour
					}
				}
			}
		}
		//判断分钟与秒
		if (timeComplete) { //添0补齐
			if (minute < 10) {
				minute = '0' + minute
			}
			if (second < 10) {
				second = '0' + second
			}
		}
		timeStr = hour + timeConnector + minute;
		//合成输出值
		if (part == 0) {
			dateStr = year + '年' + month + '月' + day + '日';
			return dateStr + ' ' + timeStr + timeConnector + second;
		} else if (part == 1) {
			dateStr = year + '年' + month + '月' + day + '日'
			return dateStr
		} else if (part == 2) {
			return timeStr + timeConnector + second;
		} else if (part == 3) {
			return timeStr;
		} else if (part == 4) {
			dateStr = month + '月' + day + '日';
			return dateStr + ' ' + timeStr;
		}
		return '传参有误'
	},
	//替换表情符号为图片
	replaceEmoji(str) {
		let replacedStr = str.replace(/\[([^(\]|\[)]*)\]/g, (item, index) => {
			console.log("item: " + item);
			for (let i = 0; i < this.emojiList.length; i++) {
				let row = this.emojiList[i];
				for (let j = 0; j < row.length; j++) {
					let EM = row[j];
					if (EM.alt == item) {
						//在线表情路径，图文混排必须使用网络路径，请上传一份表情到你的服务器后再替换此路径 
						//比如你上传服务器后，你的100.gif路径为https://www.xxx.com/emoji/100.gif 则替换onlinePath填写为https://www.xxx.com/emoji/
						let onlinePath = 'https://s2.ax1x.com/2019/04/12/'
						let imgstr = '<img src="' + onlinePath + this.onlineEmoji[EM.url] + '">';
						//let imgstr = '<img src="/static/img/emoji/' + EM.url + '">';
						console.log("imgstr: " + imgstr);
						return imgstr;
					}
				}
			}
		});
		return replacedStr;
	},

	getTimestamp() { //把时间日期转成时间戳
		return new Date().getTime() / 1000;
	},
	getCurrentTime() {
		//获取当前时间并打印
		var time;
		var date = new Date();
		let yy = date.getFullYear();
		let mm = date.getMonth() + 1;
		let dd = date.getDate();
		let hh = date.getHours();
		let mf = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
		let ss = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
		time = yy + '/' + mm + '/' + dd + ' ' + hh + ':' + mf + ':' + ss;
		return time;
	},
	//信令消息转换器
	actionConverter(actionType,data) {
		let cType = '';
		if(data){
			data = JSON.parse(data);
			let tp = data.callType;
			if( tp=='audio'){
				cType ='语音';
			}else if(tp=='video'){
				cType ='视频';
			}
		}
		switch (actionType) {
			case 1: 
				return '对方发起了'+cType+'邀请';
				break;
			case 2:
				return '对方取消了'+cType+'邀请';
				break;
			case 3:
				return '您已接受了'+cType+'邀请';
				break;
			case 4:
				return '您已拒绝了'+cType+'邀请';
				break;
			case 5:
				return cType+'邀请超时';
				break;
			default:
				return '其他';
				break;
		}
	},
	//错误码转换
	codeConverter(code) {
		switch (code) {
			case 20016:
				return '消息撤回超过了时间限制';
				break;
			case 6014:
				return '未登录，请先登录';
				break;
			case 9523:
				return '联网失败';
				break;
			case 9525:
				return '检测到没有联网';
				break;
			case 9508:
				return '网络已断开';
				break;
			case 6014:
				return '未登录或已下线';
				break;

			case 6206:
				return 'UserSig过期';
				break;
			case 6208:
				return '账号被下线';
				break;
			case 10007:
				return '您已退出群聊,无法发送信息';
				break;
			default:
				return '操作失败_' + code;
				break;
		}
	},
	//比较两个时间相差多少分钟
	compareStamp(timestamp1, timestamp2) {
		let disparity = timestamp1 - timestamp2;
		console.log(disparity);
		let min = Math.round(disparity / 1000 / 60);
		console.log(min);
		return min;
	},

	//计算语音消息长度
	soundLength(voicePlayTimes, screenWidth) {
		var barLen = 0;
		var Lmin = screenWidth * 0.2;
		var Lmax = screenWidth * 0.5;
		var barCanChangeLen = Lmax - Lmin;
		if (voicePlayTimes > 11) {
			barLen = Lmin + voicePlayTimes * 0.05 * barCanChangeLen; // VoicePlayTimes 为10秒时，正好为可变长度的一半
		} else {
			barLen = Lmin + 0.5 * barCanChangeLen + (voicePlayTimes - 10) * 0.01 * barCanChangeLen;
		}
		//console.log(screenWidth + '_' + barLen + '_' + Lmin + '_' + Lmax);
		return barLen;
	},
	//设置角标
	setBadgeNum(num) {
		tim.setBadgeNum(num);
	},
	
	//----群组相关--------//
	//获取群组消息
	getGroupHistoryMessageList(groupId,count,lastMsgId,callback){
		try {
			tim.getGroupHistoryMessageList(groupId, count, lastMsgId, res => {
				if (callback) callback(res);
			})
		} catch (res) {
			console.log(res);
			uni.showToast({
				title: JSON.stringify(res)
			})
		}
	},
	/**
	 * 设置群消息已读
	 */
	markGroupMessageAsRead(groupId,callback) {
		tim.markGroupMessageAsRead(groupId,res=>{
			if(callback)callback(res);
		})
	},
	/**
	 * 退出群组
	 */
	quitGroup(groupId,callback){
		tim.quitGroup(groupId,res=>{
			if(callback)callback(res);
		})
	},
	/**
	 * 发送 @ 信息
	 * @param {type} groupId 群组编号
	 * @param {type} text 文本 
	 * @param atStr @ 的人员
	 */
	sendAtTextMessage(groupId,text,atStr,cloudCustomData, callback) {
		tim.sendAtTextMessage(groupId,text,atStr , cloudCustomData, res => {
			console.log(res)
			if (callback) callback(res);
		})
	},
	/**
	 * 群组监听
	 */
	setGroupListener(callback){
		tim.setGroupListener(res => {
			if (callback) callback(res);
		})
	},
}
export default imCommon;
