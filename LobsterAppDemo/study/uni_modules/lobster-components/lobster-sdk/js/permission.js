/**
 * 本模块封装了Android、iOS的应用权限判断、打开应用权限设置界面、以及位置系统服务是否开启
 */

var isIos
// #ifdef APP-PLUS
isIos = (plus.os.name == "iOS")
// #endif

// 判断推送权限是否开启
function judgeIosPermissionPush() {
	var result = false;
	var UIApplication = plus.ios.import("UIApplication");
	var app = UIApplication.sharedApplication();
	var enabledTypes = 0;
	if (app.currentUserNotificationSettings) {
		var settings = app.currentUserNotificationSettings();
		enabledTypes = settings.plusGetAttribute("types");
		console.log("enabledTypes1:" + enabledTypes);
		if (enabledTypes == 0) {
			console.log("推送权限没有开启");
		} else {
			result = true;
			console.log("已经开启推送功能!")
		}
		plus.ios.deleteObject(settings);
	} else {
		enabledTypes = app.enabledRemoteNotificationTypes();
		if (enabledTypes == 0) {
			console.log("推送权限没有开启!");
		} else {
			result = true;
			console.log("已经开启推送功能!")
		}
		console.log("enabledTypes2:" + enabledTypes);
	}
	plus.ios.deleteObject(app);
	plus.ios.deleteObject(UIApplication);
	return result;
}

// 判断定位权限是否开启
function judgeIosPermissionLocation() {
	var result = false;
	var cllocationManger = plus.ios.import("CLLocationManager");
	var status = cllocationManger.authorizationStatus();
	result = (status != 2)
	console.log("定位权限开启：" + result);
	// 以下代码判断了手机设备的定位是否关闭，推荐另行使用方法 checkSystemEnableLocation
	/* var enable = cllocationManger.locationServicesEnabled();
	var status = cllocationManger.authorizationStatus();
	console.log("enable:" + enable);
	console.log("status:" + status);
	if (enable && status != 2) {
		result = true;
		console.log("手机定位服务已开启且已授予定位权限");
	} else {
		console.log("手机系统的定位没有打开或未给予定位权限");
	} */
	plus.ios.deleteObject(cllocationManger);
	return result;
}

// 判断麦克风权限是否开启
function judgeIosPermissionRecord() {
	var result = false;
	var avaudiosession = plus.ios.import("AVAudioSession");
	var avaudio = avaudiosession.sharedInstance();
	var permissionStatus = avaudio.recordPermission();
	console.log("permissionStatus:" + permissionStatus);
	if (permissionStatus == 1684369017 || permissionStatus == 1970168948) {
		console.log("麦克风权限没有开启");
	} else {
		result = true;
		console.log("麦克风权限已经开启");
	}
	plus.ios.deleteObject(avaudiosession);
	return result;
}

// 判断相机权限是否开启
function judgeIosPermissionCamera() {
	var result = false;
	var AVCaptureDevice = plus.ios.import("AVCaptureDevice");
	var authStatus = AVCaptureDevice.authorizationStatusForMediaType('vide');
	console.log("authStatus:" + authStatus);
	// uni.showToast({
	// 	title:'authStatus:' + authStatus
	// })
	if (authStatus == 3) {
		result = true;
		console.log("相机权限已经开启");
	} else {
		console.log("相机权限没有开启");
	}
	plus.ios.deleteObject(AVCaptureDevice);
	return result;
}

// 判断相册权限是否开启
function judgeIosPermissionPhotoLibrary() {
	var result = false;
	var PHPhotoLibrary = plus.ios.import("PHPhotoLibrary");
	var authStatus = PHPhotoLibrary.authorizationStatus();
	console.log("authStatus:" + authStatus);
	if (authStatus == 3) {
		result = true;
		console.log("相册权限已经开启");
	} else {
		console.log("相册权限没有开启");
	}
	plus.ios.deleteObject(PHPhotoLibrary);
	return result;
}

// 判断通讯录权限是否开启
function judgeIosPermissionContact() {
	var result = false;
	var CNContactStore = plus.ios.import("CNContactStore");
	var cnAuthStatus = CNContactStore.authorizationStatusForEntityType(0);
	if (cnAuthStatus == 3) {
		result = true;
		console.log("通讯录权限已经开启");
	} else {
		console.log("通讯录权限没有开启");
	}
	plus.ios.deleteObject(CNContactStore);
	return result;
}

// 判断日历权限是否开启
function judgeIosPermissionCalendar() {
	var result = false;
	var EKEventStore = plus.ios.import("EKEventStore");
	var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(0);
	if (ekAuthStatus == 3) {
		result = true;
		console.log("日历权限已经开启");
	} else {
		console.log("日历权限没有开启");
	}
	plus.ios.deleteObject(EKEventStore);
	return result;
}

// 判断备忘录权限是否开启
function judgeIosPermissionMemo() {
	var result = false;
	var EKEventStore = plus.ios.import("EKEventStore");
	var ekAuthStatus = EKEventStore.authorizationStatusForEntityType(1);
	if (ekAuthStatus == 3) {
		result = true;
		console.log("备忘录权限已经开启");
	} else {
		console.log("备忘录权限没有开启");
	}
	plus.ios.deleteObject(EKEventStore);
	return result;
}

// Android权限查询
function requestAndroidPermission(permissionID) {
	return new Promise((resolve, reject) => {
		plus.android.requestPermissions(
			[permissionID], // 理论上支持多个权限同时查询，但实际上本函数封装只处理了一个权限的情况。有需要的可自行扩展封装
			function(resultObj) {
				var result = 0;
				for (var i = 0; i < resultObj.granted.length; i++) {
					var grantedPermission = resultObj.granted[i];
					console.log('已获取的权限：' + grantedPermission);
					result = 1
				}
				for (var i = 0; i < resultObj.deniedPresent.length; i++) {
					var deniedPresentPermission = resultObj.deniedPresent[i];
					console.log('拒绝本次申请的权限：' + deniedPresentPermission);
					result = 0
				}
				for (var i = 0; i < resultObj.deniedAlways.length; i++) {
					var deniedAlwaysPermission = resultObj.deniedAlways[i];
					console.log('永久拒绝申请的权限：' + deniedAlwaysPermission);
					result = -1
				}
				resolve(result);
				// 若所需权限被拒绝,则打开APP设置界面,可以在APP设置界面打开相应权限
				// if (result != 1) {
				// gotoAppPermissionSetting()
				// }
			},
			function(error) {
				uni.showModal({
					title: "requestAndroidPermission",
					content: JSON.stringify(error)
				})
				console.log('申请权限错误：' + error.code + " = " + error.message);
				
				resolve({
					code: error.code,
					message: error.message
				});
			}
		);
	});
}

// 使用一个方法，根据参数判断权限
function judgeIosPermission(permissionID) {
	if (permissionID == "location") {
		return judgeIosPermissionLocation()
	} else if (permissionID == "camera") {
		return judgeIosPermissionCamera()
	} else if (permissionID == "photoLibrary") {
		return judgeIosPermissionPhotoLibrary()
	} else if (permissionID == "record") {
		return judgeIosPermissionRecord()
	} else if (permissionID == "push") {
		return judgeIosPermissionPush()
	} else if (permissionID == "contact") {
		return judgeIosPermissionContact()
	} else if (permissionID == "calendar") {
		return judgeIosPermissionCalendar()
	} else if (permissionID == "memo") {
		return judgeIosPermissionMemo()
	}
	return false;
}


const permList = [];
async function auth(permissionID, firstFun) { 
	if (permList.indexOf(permissionID) >= 0) {
		return 1;
	}
	let alertTit = ""
	if (isIos) {
		//特殊处理
		if (permissionID == 'cameraandrecord') {
			let isFirstCamera = uni.getStorageSync('p_camera');
			let isFirstRecord = uni.getStorageSync('p_record');
			let camera1 = judgeIosPermission("camera"); //判断ios是否给予摄像头权限
			let mic1 = judgeIosPermission('record'); //判断ios是否给予麦克风权限
			if ((!isFirstCamera && !isFirstRecord) || (!isFirstCamera && mic1) || (!isFirstRecord && camera1)) {
				uni.setStorageSync('p_camera', permissionID);
				uni.setStorageSync('p_record', permissionID);
				firstFun && firstFun()
			} else {
				if (camera1 && mic1) {
					permList.push(permissionID);
					return 1;
				}
				if (!camera1 && !mic1) {
					alertTit = '需要开启麦克风和相机权限';
				} else {
					if (!mic1) {
						alertTit = '需要开启麦克风权限';
					}
					if (!camera1) {
						alertTit = '需要开启相机权限';
					}
				}
			}
		} else {
			var isFirst = uni.getStorageSync('p_' + permissionID);
			if (!isFirst) {
				uni.setStorageSync('p_' + permissionID, permissionID);
				firstFun && firstFun()
			} else {
				switch (permissionID) {
					case "scan":
						let scan = judgeIosPermission("camera"); //判断ios是否给予摄像头权限
						if (scan) {
							permList.push(permissionID);
							return 1;
						} else {
							alertTit = '需要开启相机权限';
						}
						break;
					case "camera":
						let camera = judgeIosPermission("camera"); //判断ios是否给予摄像头权限
						if (camera) {
							permList.push(permissionID);
							return 1;
						} else {
							alertTit = '需要开启相机权限';
						}
						break;
					case 'photoLibrary':
						let photoLibrary = judgeIosPermission("photoLibrary"); //判断ios是否给予相册权限
						if (photoLibrary) {
							permList.push(permissionID);
							return 1;
						} else {
							alertTit = '需要开启相册权限'
						}
						break;
					case 'record':
						let mic2 = judgeIosPermission('record');
						if (mic2) {
							permList.push(permissionID);
							return 1;
						} else {
							alertTit = '需要开启麦克风权限';
						}
						break;
					default:
						break;
				}
			}
		}
	} else {
		if (permissionID == "location") { // 位置
			var result = await requestAndroidPermission('android.permission.ACCESS_FINE_LOCATION');
			if (result && result == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启摄像头和麦克风权限';
			}
		} else if (permissionID == "camera") { //摄像头与麦克风
			var camera = await requestAndroidPermission('android.permission.CAMERA');
			var audio = await requestAndroidPermission('android.permission.RECORD_AUDIO');
			if (camera && camera == 1 && audio && audio == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启摄像头和麦克风权限';
			}
		} else if (permissionID == "cameraandrecord") { //摄像头与麦克风 
			var camera = await requestAndroidPermission('android.permission.CAMERA');
			var audio = await requestAndroidPermission('android.permission.RECORD_AUDIO'); 
			if (camera && camera == 1 && audio && audio == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启摄像头和麦克风权限';
			}
		} else if (permissionID == "photoLibrary") { //相册
			var read = await requestAndroidPermission('android.permission.READ_EXTERNAL_STORAGE');
			var write = await requestAndroidPermission('android.permission.WRITE_EXTERNAL_STORAGE');
			if (read && read == 1 && write && write == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启相册权限';
			}
		} else if (permissionID == "record") { //麦克风
			var result = await requestAndroidPermission('android.permission.RECORD_AUDIO');
			if (result && result == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启麦克风权限'
			}
		} else if (permissionID == 'scan') {
			var scan = await requestAndroidPermission('android.permission.CAMERA');
			if (scan && scan == 1) {
				permList.push(permissionID);
				return 1;
			} else {
				alertTit = '需要开启摄像头';
			}
		} else if (permissionID == "push") {} else if (permissionID == "contact") {} else if (permissionID ==
			"calendar") {} else if (permissionID == "memo") {}
	}
	if (alertTit) {
		uni.showModal({
			title: alertTit,
			success(res) {
				if (res.confirm)
					gotoAppPermissionSetting()
			}
		})
	}
	return 0;
}

function permisionIosSync(permissionID) {
	return new Promise((rel, rej) => {
		let permission = judgeIosPermission(permissionID);
		if (permission) {
			rel();
		} else {
			rej('未开启权限');
		}
	})
}

// 跳转到**应用**的权限页面
function gotoAppPermissionSetting() {
	if (isIos) {
		var UIApplication = plus.ios.import("UIApplication");
		var application2 = UIApplication.sharedApplication();
		var NSURL2 = plus.ios.import("NSURL");
		// var setting2 = NSURL2.URLWithString("prefs:root=LOCATION_SERVICES");		
		var setting2 = NSURL2.URLWithString("app-settings:");
		application2.openURL(setting2);

		plus.ios.deleteObject(setting2);
		plus.ios.deleteObject(NSURL2);
		plus.ios.deleteObject(application2);
	} else {
		// console.log(plus.device.vendor);
		var Intent = plus.android.importClass("android.content.Intent");
		var Settings = plus.android.importClass("android.provider.Settings");
		var Uri = plus.android.importClass("android.net.Uri");
		var mainActivity = plus.android.runtimeMainActivity();
		var intent = new Intent();
		intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		var uri = Uri.fromParts("package", mainActivity.getPackageName(), null);
		intent.setData(uri);
		mainActivity.startActivity(intent);
	}
}

// 检查系统的设备服务是否开启
// var checkSystemEnableLocation = async function () {
function checkSystemEnableLocation() {
	if (isIos) {
		var result = false;
		var cllocationManger = plus.ios.import("CLLocationManager");
		var result = cllocationManger.locationServicesEnabled();
		console.log("系统定位开启:" + result);
		plus.ios.deleteObject(cllocationManger);
		return result;
	} else {
		var context = plus.android.importClass("android.content.Context");
		var locationManager = plus.android.importClass("android.location.LocationManager");
		var main = plus.android.runtimeMainActivity();
		var mainSvr = main.getSystemService(context.LOCATION_SERVICE);
		var result = mainSvr.isProviderEnabled(locationManager.GPS_PROVIDER);
		console.log("系统定位开启:" + result);
		return result
	}
}

module.exports = {
	judgeIosPermission: judgeIosPermission,
	requestAndroidPermission: requestAndroidPermission,
	checkSystemEnableLocation: checkSystemEnableLocation,
	gotoAppPermissionSetting: gotoAppPermissionSetting,
	auth: auth,
}
