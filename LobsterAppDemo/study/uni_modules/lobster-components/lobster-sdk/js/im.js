const im = {
	state: {
		isLogin: false,
		isSDKReady: false, // TIM SDK 是否 ready 
		messageList: [], //消息列表
		currNewMessageList: [], //当前会话记录列表
		toUserId: '', //当前会话
		imUpdateIndex: 1, //im数据更新标识
		floatCall: null,
		ing: false,
		freshConversation:false, //刷新会话
	},
	getters: {
		isLogin: state => state.isLogin,
		isSDKReady: state => state.isSDKReady,
		messageList: state => state.messageList,
		ing: state => state.ing,
		freshConversation:state=>state.freshConversation,
	},
	mutations: {
		//更新是否在房间
		toggleIng(state, ing) {
			state.ing = typeof ing === 'undefined' ? !state.ing : ing
		},
		//更新登录状态
		toggleIsLogin(state, isLogin) {
			state.isLogin = typeof isLogin === 'undefined' ? !state.isLogin : isLogin
		},
		//更新TIMSDK状态
		toggleIsSDKReady(state, isSDKReady) {
			state.isSDKReady = typeof isSDKReady === 'undefined' ? !state.isSDKReady : isSDKReady
		},
		toggleFresh(state, freshConversation) {
			state.freshConversation = typeof freshConversation === 'undefined' ? !state.freshConversation : freshConversation
		},
		//退出登录重置状态
		reset(state) {
			state.isLogin = false
			state.isSDKReady = false
		},
		//更新会话，把messageList中的数据同步到当前会话，避免会话退出在会话中调用
		toggleCurrentActive(state, toUserId) {
			console.log('更新')
			let list = [];
			console.log(state.messageList);
			for (let i = 0; i < state.messageList.length; i++) {
				if (!state.messageList[i].groupID &&( state.messageList[i].sender == toUserId || state.messageList[i].userID == toUserId)) {
					list.push(state.messageList[i]);
					state.messageList.splice(i, 1);
					i--;
				}
			}
			state.currNewMessageList = list;
		},
		
		//群组
		toggleGroupCurrentActive(state, groupId) {
			let list = [];
			console.log(state.messageList);
			for (let i = 0; i < state.messageList.length; i++) {
				if (state.messageList[i].groupID == groupId ) {
					list.push(state.messageList[i]);
					state.messageList.splice(i, 1);
					i--;
				}
			}
			state.currNewMessageList = list; 
		},
		//选择好友聊天--创建会话/拼接会话id
		createConversationActive(state, toUserId) {
			state.toUserId = toUserId
			state.currNewMessageList = []
		},

		//更新会话列表
		updateConversationList(state, newConversationList) {
			state.conversationList = newConversationList
		},
		/**
		 * 将消息插入当前会话列表
		 * 调用时机：收/发消息事件触发时
		 * @param {Object} state
		 * @param {Message[]|Message} data
		 * @returns
		 */
		pushCurrentMessageList(state, data) {
			if (Array.isArray(data)) {
				// 筛选出当前会话的消息
				state.messageList.push(...data);
			} else {
				state.messageList.push(data);
			}
			state.imUpdateIndex = state.imUpdateIndex + 1 ;
		},



		/**
		 * 滑到顶部请求更多的历史消息
		 * */
		unshiftCurrentMessageList(state, data) {
			if (data) {
				state.messageList = [...data, ...state.messageList]
			}
		},
		// 重置当前会话
		resetCurrentConversation(state, toUserId) {
			//state.messageList = []
			for (let i = 0; i < state.messageList.length; i++) {
				if (state.messageList[i].sender == toUserId || state.messageList[i].userID == toUserId) {
					state.messageList.splice(i, 1);
					i--;
				}
			}
			state.currNewMessageList = []
		},
		
		// 重置当前会话
		resetGroupCurrentConversation(state, groupId) {
			//state.messageList = []
			for (let i = 0; i < state.messageList.length; i++) {
				if (state.messageList[i].groupID == groupId ) {
					state.messageList.splice(i, 1);
					i--;
				}
			}
			state.currNewMessageList = []
		},

	},

	actions: {

	}
}
export default im
