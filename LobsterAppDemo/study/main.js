import App from './App'

import store from './store'
import util from '@/uni_modules/lobster-components/lobster-sdk/js/util.js'
import  lobster from '@/uni_modules/lobster-components/lobster-sdk/lobster.js'

Vue.prototype.$util = util;
Vue.prototype.$store = store;
Vue.prototype.$lobster = lobster;

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
	store,
    app
  }
}
// #endif