﻿using BookApp.Dao;
using BookApp.Entity;
using Lobster.Core.Common.CoreFrame;
using Lobster.Core.Common.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookApp.Controllers
{
    [ApiVersion("1.0")]
    [Route("book/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    public class BookController: ApiControllerBase
    {
        [HttpGet]
        //[AllowAnonymous]
        public object GetSimpleData(int page, int limit)
        {
            PageInfo pageinfo = new PageInfo(limit, page);
            pageinfo.OrderBy = new string[] { "数据元标识符" };
            var data = NewDao<BookDao>().GetData(ref pageinfo);
            return ToTableJson(pageinfo.totalRecord, data);
        }

        [HttpGet]
        public object getbooks(int page, int limit, string keywords, int delflag)
        {
            PageInfo pageinfo = new PageInfo(limit, page);
            pageinfo.OrderBy = new string[] { "Id" };

            var list = NewDao<BookDao>().getbooks(ref pageinfo,keywords,delflag);

            return ToTableJson(pageinfo.totalRecord, list);
        }

        [HttpPost]
        public object savebook(BookEntity book)
        {
            bool result = Save<BookEntity>(book);
            AddData("result", result);
            return Success();
        }

        [HttpPost]
        public object deletebook(dynamic data)
        {
            bool result = Delete<BookEntity>(Convert.ToInt32(data.Id));
            AddData("result", result);
            return Success();
        }

        private string ToTableJson(int count, object model)
        {
            string rowjson = JsonConvert.SerializeObject(model);
            string json = "{ \"code\":0,\"msg\":\"\",\"count\":" + count + ",\"data\":" + rowjson + "}";
            return json;
        }
    }
}
