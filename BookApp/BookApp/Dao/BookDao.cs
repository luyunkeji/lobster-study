﻿using Dapper;
using Lobster.Core.Common.CoreFrame;
using Lobster.Core.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookApp.Dao
{
    public class BookDao: AbstractDao
    {
        public dynamic GetData(ref PageInfo page)
        {
            string strsql = @"SELECT * FROM 数据元";
            strsql = SqlPage.FormatSql(DatabaseType.SqlServer, strsql, page, connection);
            return connection.Query(strsql);
        }

        public IEnumerable<dynamic> getbooks(ref PageInfo page, string keywords, int delflag)
        {
            string strsql = @"select * from books where BookName like @keywords and Flag=@delflag";
            strsql = SqlPage.FormatSql(DatabaseType.SqlServer, strsql, page, connection, new { keywords = "%" + keywords + "%", delflag });
            return connection.Query(strsql, new { keywords = "%" + keywords + "%", delflag });
        }
    }
}
