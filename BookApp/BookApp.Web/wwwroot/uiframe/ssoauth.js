﻿var ssotoken = localStorage.getItem("sso_token");
//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}
if (getUrlParam("ssotoken")) {
    ssotoken = getUrlParam("ssotoken");
}

var url = window.location.pathname;//对当前页面进行鉴权

//获取站点配置
function VerifyToken(callback) {
    $.ajax({
        url: "/Login/VerifyToken",
        dataType: "json",
        data: { ssotoken: ssotoken, url: url },
        success: function (res) {
            if (res.code == 0) {
                //缓存到浏览器token、right
                localStorage.setItem("sso_token", res.data.ssotoken);
                localStorage.setItem("sso_appkey", res.data.appkey);
                localStorage.setItem("sso_right", res.data.right);
                if (callback) {
                    callback();//最后才执行页面的脚本
                }
            }
            else {
                //移除token、right
                localStorage.removeItem("sso_token");
                localStorage.removeItem("sso_appkey");
                localStorage.removeItem("sso_right");

                if (parent) {
                    parent.window.location.href = "/Login/Index";
                } else {
                    window.location.href = "/Login/Index";
                }
            }
        },
        done: function () {

        }
    });
}
